#!/usr/bin/python3


# =============================================================================
# Tivom is a multi-platform application for creating vector images. It is not
# finished and it is being developed right now.
# =============================================================================


# Header
__author__ = "William Tatarko"
__email__ = "william.tatarko@gmail.com"
__version__ = "0.0.1"


# Import gi and check if its version is correct
import gi
gi.require_version("Gtk", "3.0")

# Import Gtk, Gdk and GObject
# - Gtk is used as a window manager
# - Gdk handles keyboard input
# - GObject is used for continues updates
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GObject

# Import Cairo
# - Cairo is used for all graphics
import cairo

# Other imports
# - math is used for goniometric/cyclometric function, square roots etc.
# - random is used for random ray direction in face selection
# - pickle is used for saving/loading projects
# - os.path is used for checking if project already exists
# - colorsys is used for converting rgb to hsv and vice versa
# - copy is used for coping objects from/to history
import math
import random
import pickle
import os.path
import colorsys
import copy


# =============================================================================
# This is the main class. It creates the window, contains all objects and also
# handles input events.
# =============================================================================
class Window(Gtk.Window):
	# Sets initial values and connects events to their appropriate functions
	def __init__(self):
		super().__init__()

		# Set window title
		self.set_title("Tivom")
		self.resize(640, 640)
		self.set_position(Gtk.WindowPosition.CENTER)

		# Delay (ms) between frames
		# TODO: Variable "delta_time" is problematic. Usually there is no
		# problem, but if the window contains too many objects and therefor it
		# is not able to render it quickly (it takes longer than "delta_time"),
		# black screen will occur. This is the reason, why "delta_time" has
		# been - as a temporary solution - introduced, because if we used 1 ms,
		# screen would be black even with not so much objects drawn on it.
		delta_time = 20

		# Set initial shift. Shift is position of top left corner of the canvas
		# on the screen.
		self.shift = (0, 0)

		# Set initial zoom. 1 means real size, 2 for example means that line
		# with real lenght 10 pixels will be displayed as line with lenght 20.
		self.zoom = 1

		# Mode defines how the app should response to specific actions
		self.mode = "welcome"

		# History is used for storing every step, so it can be restored lately.
		# It is a list of lists containing all objects. History index indicates
		# where in history we are. If history list has lenght 5 and our history
		# index is 3 (so we are on fourth list) we can go 3 steps further back
		# in the history or redo one time.
		self.history = [[]]
		self.history_index = 0

		# Screen mouse position (x, y) on the last frame. Coordinates (0, 0)
		# means top left corner.
		self.last_pos = self.get_pointer()

		# List of all keys, which are being held. The list contains key IDs
		# according to this page:
		# - https://lazka.github.io/pgi-docs/#Gdk-3.0/constants.html
		self.held_keys = []

		# List of all objects on the screen. Contains objects of type "Object".
		self.all_objects = []

		# List of all active objects. Contations only indices of the active
		# objects for list "all_objects".
		self.active_objects = []

		# Create color dialog
		self.color_dialog = ColorDialog()

		# Create drawing area and add it to the Window
		drawing_area = Gtk.DrawingArea()
		self.add(drawing_area)

		# Call "draw" function whenever drawing is needed
		drawing_area.connect("draw", self.draw)
		
		# Call "refresh" function every "delta_time" milliseconds
		GObject.timeout_add(delta_time, self.refresh)

		# Call "on_click" function if mouse is clicked or released
		self.connect("button-press-event", self.on_mouse_click)
		self.connect("button-release-event", self.on_mouse_release)

		# Call functions if some key is pressed or released
		self.connect("key-press-event", self.on_key_press)
		self.connect("key-release-event", self.on_key_release)

		# Stop the application if window is closed
		self.connect("delete-event", self.quit)

		# Display the Window
		self.show_all()

	# All draw functions ======================================================
	
	# Function "draw" is called whenever the widget "Gtk.DrawingArea" is asked
	# to redraw itself. This function is called using "queue_draw" function,
	# which handles the drawing event.
	# - "drawing_area" is the widget "Gtk.DrawingArea" (not used)
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	def draw(self, drawing_area, ctx):
		if self.mode == "welcome":
			self.draw_background(ctx)
			self.draw_title(ctx)

		else:
			# Set ending of lines to be rounded
			ctx.set_miter_limit(cairo.LINE_JOIN_ROUND)
			ctx.set_line_cap(cairo.LINE_CAP_ROUND)

			# Draw different parts of the screen
			self.draw_background(ctx)
			self.draw_canvas(ctx)
			self.draw_objects(ctx)
			self.draw_color_dialog(ctx)
			self.draw_selection_box(ctx)
			self.draw_canvas_border(ctx)

	# Draws the title for the welcome screen
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	def draw_title(self, ctx):
		# If file with the logo exists
		if os.path.isfile("./title.png"):
			# Get "title.png" as "ImageSurface" so it can be later displayed
			title = cairo.ImageSurface.create_from_png("./title.png")
			
			# Get screen size
			screen_size = self.get_size()

			# Get dimensions of the title
			title_width = title.get_width()
			title_height = title.get_height()

			# How much of the screen width should be taken by the title
			ratio_width = 0.6
			
			# How much the title needs to be scaled in order to satisfy the
			# "ratio_width"
			scale_ratio = (ratio_width * screen_size[0]) / title_width

			# Check if height of the title after scaling is not to much
			if title_height * scale_ratio > 0.8 * screen_size[1]:
				# Modify the "scale_ratio" if needed
			 	scale_ratio = (0.8 * screen_size[1]) / title_height

			# Calculate position of the title, so it could be in the center
			left = (screen_size[0] - scale_ratio * title_width) / 2
			top = (screen_size[1] - scale_ratio * title_height) / 2

			# Displayed the title as planned
			ctx.translate(left, top)
			ctx.scale(scale_ratio, scale_ratio)
			ctx.set_source_surface(title)
			ctx.paint()

	# Draws the background
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	def draw_background(self, ctx):
		# Set color
		ctx.set_source_rgb(0.3, 0.3, 0.3)

		# Fill the whole current region (entire screen)
		ctx.paint()

	# Draws the canvas
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	def draw_canvas(self, ctx):
		# Set color
		ctx.set_source_rgb(1, 1, 1)

		# Define canvas rectangle
		ctx.rectangle(self.shift[0], self.shift[1], self.width * self.zoom,
			self.height * self.zoom)
		
		# Fill the rectangle with the color
		ctx.fill()

	# Draws all objects
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	def draw_objects(self, ctx):
		for i, object in enumerate(self.all_objects):
			object.draw(ctx, i in self.active_objects, self.mode, self.shift,
						self.zoom, True)

	# Draws the color dialog
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	def draw_color_dialog(self, ctx):
		# If color dialog shuold be displayed
		if (self.mode == "color_dialog" or
			self.mode == "changing_hue" or
			self.mode == "changing_saturation" or
			self.mode == "changing_value" or
			self.mode == "changing_alpha"):

			# Select the active object (since we are in edit-based mode)
			index = self.active_objects[0]
			object = self.all_objects[index]

			# Initialize colors
			r, g, b, a = 0, 0, 0, 0

			# If faces are selected, calculate their total color
			if len(object.active_faces) > 0:
				for index in object.active_faces:
					face = object.all_faces[index]
					rgba = face.color_inactive
					r += rgba[0]
					g += rgba[1]
					b += rgba[2]
					a += rgba[3]
				nu_parts = len(object.active_faces)

			# If edges are selected, calculate their total color
			elif len(object.active_edges) > 0:
				for index in object.active_edges:
					edge = object.all_edges[index]
					rgba = edge.color_real
					r += rgba[0]
					g += rgba[1]
					b += rgba[2]
					a += rgba[3]
				nu_parts = len(object.active_edges)

			# Get the average color of faces/edges
			r /= nu_parts
			g /= nu_parts
			b /= nu_parts
			a /= nu_parts
			rgba = (r, g, b, a)

			# Draw the dialog itself
			self.color_dialog.draw(ctx, rgba)

	# Draws the selection box
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	def draw_selection_box(self, ctx):
		# Selection box is drawn only while "selection_box" mode is on
		if self.mode == "selecting_box":
			# Set line properties
			ctx.set_dash([5])
			ctx.set_source_rgba(0, 0, 0, 1)
			ctx.set_line_width(1)

			# Screen start position
			s_x = self.shift[0] + self.selection_box_start[0] * self.zoom
			s_y = self.shift[1] + self.selection_box_start[1] * self.zoom

			# Screen end position
			e_x = self.shift[0] + self.selection_box_end[0] * self.zoom
			e_y = self.shift[1] + self.selection_box_end[1] * self.zoom

			# Get x-coordinate of top left corner and width
			if s_x <= e_x:
				a_x = s_x
				width = e_x - s_x
			else:
				a_x = e_x
				width = s_x - e_x

			# Get y-coordinate of top left corner and height
			if s_y <= e_y:
				a_y = s_y
				height = e_y - s_y
			else:
				a_y = e_y
				height = s_y - e_y

			# Draw the selection box itself
			ctx.rectangle(a_x, a_y, width, height)
			ctx.stroke()

			# Reset dash settings
			ctx.set_dash([])

	# Draws border of the canvas
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	def draw_canvas_border(self, ctx):
		# Change full lines to dashed lines
		ctx.set_dash([5])

		# Set width
		ctx.set_line_width(1)

		# Set color
		ctx.set_source_rgb(1, 1, 1)

		# Define the rectangle
		ctx.rectangle(self.shift[0], self.shift[1], self.width * self.zoom,
			self.height * self.zoom)

		# Draw
		ctx.stroke()

		# Change dashed lines to full lines
		ctx.set_dash([])
	
	# Functions handling mouse movement =======================================

	# Function "refresh" is called every "delta_time" milliseconds. It is
	# mainly used for handling mouse movement while modes like "moving_objects"
	# and similar are active.
	def refresh(self):
		# Get current mouse position (x, y)
		new_pos = self.get_pointer()

		# Calculate the horizontal and vertical difference between "new_pos"
		# and "last_pos"
		dx = new_pos[0] - self.last_pos[0]
		dy = new_pos[1] - self.last_pos[1]

		# Handle mouse movement depending on the mode
		if self.mode == "moving_objects":
			self.moving_objects(dx, dy)

		elif self.mode == "moving_points":
			self.moving_points(dx, dy)

		elif self.mode == "rotating_points":
			self.rotating_points(new_pos)

		elif self.mode == "moving_bezier_control_point":
			self.moving_bezier_control_point(dx, dy)

		elif self.mode == "changing_hue":
			self.changing_hue(new_pos[0])

		elif self.mode == "changing_saturation":
			self.changing_saturation(new_pos[0])

		elif self.mode == "changing_value":
			self.changing_value(new_pos[0])

		elif self.mode == "changing_alpha":
			self.changing_alpha(new_pos[0])

		elif self.mode == "scaling_points":
			self.scaling_points(new_pos)

		elif self.mode == "moving_canvas":
			self.moving_canvas(dx, dy)

		elif self.mode == "scaling_canvas":
			self.scaling_canvas(new_pos)

		elif self.mode == "selecting_box":
			self.selection_box_end = self.screen_to_canvas_pos(new_pos)

		# Make from the "new_pos" the "last_pos" for next frame
		self.last_pos = new_pos

		# This calls the main draw function, which redraw the entire screen
		self.queue_draw()

		# Return True. Otherwise the refresh function would not be called again
		return True

	# Moves with one or more objects according to mouse movement
	# - "dx" is horizontal change of mouse position between current and last
	#	frame
	# - "dy" is vertical change of mouse position between current and last
	#	frame
	def moving_objects(self, dx, dy):
		# More objects can be moved at once. Therefore there need to be a
		# for-cycle.
		for i, object in enumerate(self.all_objects):
			# If object "object" with index "i" is active
			if i in self.active_objects:
				# Calculate new position of the object center
				x = object.center[0] + float(dx)/self.zoom
				y = object.center[1] + float(dy)/self.zoom

				# Update position of the object center
				object.center = (x, y)

	# Moves with one or more points according to mouse movement
	# - "dx" is horizontal change of mouse position between current and last
	#	frame
	# - "dy" is vertical change of mouse position between current and last
	#	frame
	def moving_points(self, dx, dy):
		# Mode "moving_points" can be activated only from "edit" mode,
		# which can be entered only if "active_objects" contains only one
		# index. Therefore "active_objects[0]" is index of the active
		# object.
		index = self.active_objects[0]

		# Access to the object itself using "index"
		object = self.all_objects[index]

		# More points can be moved at once. Therefore there need to be a
		# for-cycle.
		for i, point in enumerate(object.all_points):
			# If point "point" of object "object" is active
			if i in object.active_points:
				# Get point position (relative to the object center)
				pos = point.pos

				# Calculate new position of the point
				x = pos[0] + float(dx)/self.zoom
				y = pos[1] + float(dy)/self.zoom

				# Update point coordinates
				point.pos = (x, y)

	# Rotates with all selected points around the object center according to
	# mouse movement
	# - "new_pos" is currect mouse position on the screen
	def rotating_points(self, new_pos):
		# Mode "rotating_points" can be activated only from "edit" mode,
		# which can be entered only if "active_objects" contains only one
		# index. Therefore "active_objects[0]" is index of the active
		# object.
		index = self.active_objects[0]

		# Access to the object itself using "index"
		object = self.all_objects[index]

		# Calculate the horizontal and vertical difference between
		# "last_pos" and object's center
		center_x = self.shift[0] + object.center[0] * self.zoom
		center_y = self.shift[1] + object.center[1] * self.zoom

		x = self.last_pos[0] - center_x
		y = self.last_pos[1] - center_y

		# Calculate the "last_angle" between line from "object.center" to
		# "last_pos" and line starting at "object.center" with direction
		# vector (1, 0)
		last_angle = object.calculate_angle(x, y)

		# Calculate the horizontal and vertical difference between
		# "new_pos" and object's center
		x = new_pos[0] - center_x
		y = new_pos[1] - center_y

		# Calculate the "new_angle" between line from "object.center" to
		# "new_pos" and line starting at "object.center" with direction
		# vector (1, 0)
		new_angle = object.calculate_angle(x, y)

		# More points can be rotated at once. Therefore there need to be a
		# for-cycle.
		for i, point in enumerate(object.all_points):
			# If point "point" of object "object" is active
			if i in object.active_points:
				# Get point position (relative to the object center)
				pos = point.pos

				# Position of every point is relative to the object center.
				# Therefore in order to calculate distance between some
				# point and the object center we need to calculate the
				# distance from those relative coordinates to point (0, 0).
				distance = self.distance(pos, (0, 0))

				# Calculate angle between vector from (0, 0) to
				# coordinates of "point" and direction vector (1, 0)
				angle = object.calculate_angle(*pos)

				# Update angle of the "point" with respect to the
				# difference between "new_angle" and "last_angle"
				angle += new_angle - last_angle

				# Calculate new relative (to the object center) position of
				# the point using known distance (which does not change),
				# the new angle and goniometric function 
				x = distance * math.cos(angle)
				y = distance * math.sin(angle)

				# Update position of the point
				point.pos = (x, y)

				# Rotate also Bezier control points if necessary
				for edge in object.all_edges:
					if edge.type == "bezier":
						if edge.a == i:
							# Get Bezier control center canvas position
							# relative to the object center
							bcp_pos = object.absolute(edge.c.pos, pos)

							# Calculate distance from the Bezier control point
							# to the object center
							bcp_distance = self.distance(bcp_pos, (0, 0))
							
							# Get angle before the rotation
							bcp_angle = object.calculate_angle(*bcp_pos)

							# Calculate new angle after rotation
							bcp_angle += new_angle - last_angle

							# Calculate new Cezier control point coordinates
							# relative to the object center
							bcp_x = bcp_distance * math.cos(bcp_angle)
							bcp_y = bcp_distance * math.sin(bcp_angle)

							# Set new Bezier control point position relative to
							# the center
							bcp_pos = (bcp_x, bcp_y)

							# Set new Bezier control position relative to the
							# point
							edge.c.pos = object.relative(bcp_pos, point.pos)
						
						if edge.b == i:
							# Get Bezier control center canvas position
							# relative to the object center
							bcp_pos = object.absolute(edge.d.pos, pos)

							# Calculate distance from the Bezier control point
							# to the object center
							bcp_distance = self.distance(bcp_pos, (0, 0))

							# Get angle before the rotation
							bcp_angle = object.calculate_angle(*bcp_pos)

							# Calculate new angle after rotation
							bcp_angle += new_angle - last_angle

							# Calculate new Cezier control point coordinates
							# relative to the object center
							bcp_x = bcp_distance * math.cos(bcp_angle)
							bcp_y = bcp_distance * math.sin(bcp_angle)

							# Set new Bezier control point position relative to
							# the center
							bcp_pos = (bcp_x, bcp_y)

							# Set new Bezier control position relative to the
							# point
							edge.d.pos = object.relative(bcp_pos, point.pos)

	# Moves control point of Bezier curve according to mouse movement
	# - "dx" is horizontal change of mouse position between current and last
	#	frame
	# - "dy" is vertical change of mouse position between current and last
	#	frame
	def moving_bezier_control_point(self, dx, dy):
		index = self.active_objects[0]
		object = self.all_objects[index]
		object.move_bezier_control_point(float(dx)/self.zoom,
			float(dy)/self.zoom)

	# Changes hue (and moves with its cursor) according to mouse movement
	# - "new_x" is horizontal screen position of mouse
	def changing_hue(self, new_x):
		# Select object
		index = self.active_objects[0]
		object = self.all_objects[index]
		
		# Get new value of hue
		new_hue = self.color_dialog.change_hue(new_x)

		# Change hue of the object to the new value
		object.change_hue(new_hue)

	# Changes saturation (and moves with its cursor) according to mouse
	# movement
	# - "new_x" is horizontal screen position of mouse
	def changing_saturation(self, new_x):
		# Select object
		index = self.active_objects[0]
		object = self.all_objects[index]
		
		# Get new value of saturation
		new_saturation = self.color_dialog.change_saturation(new_x)

		# Change saturation of the object to the new value
		object.change_saturation(new_saturation)

	# Changes value (and moves with its cursor) according to mouse movement
	# - "new_x" is horizontal screen position of mouse
	def changing_value(self, new_x):
		# Select object
		index = self.active_objects[0]
		object = self.all_objects[index]
		
		# Get new value of value
		new_value = self.color_dialog.change_value(new_x)

		# Change value of the object to the new value
		object.change_value(new_value)

	# Changes value (and moves with its cursor) according to mouse movement
	# - "new_x" is horizontal screen position of mouse
	def changing_alpha(self, new_x):
		# Select object
		index = self.active_objects[0]
		object = self.all_objects[index]
		
		# Get new value of alpha
		new_alpha = self.color_dialog.change_alpha(new_x)

		# Change alpha of the object to the new value
		object.change_alpha(new_alpha)

	# Moves all active points towards or away from the object center depending
	# on the mouse motion
	# - "new_pos" is current screen position of mouse
	def scaling_points(self, new_pos):
		# Select object
		index = self.active_objects[0]
		object = self.all_objects[index]

		# Get canvas mouse position on current and last frame
		last_canvas_pos = self.screen_to_canvas_pos(self.last_pos)
		new_canvas_pos = self.screen_to_canvas_pos(new_pos)

		# Scale points
		object.scaling_points(last_canvas_pos, new_canvas_pos)

	# Moves whole canvas with all objects on it
	# - "dx" is horizontal change of mouse position between current and last
	#	frame
	# - "dy" is vertical change of mouse position between current and last
	#	frame
	def moving_canvas(self, dx, dy):
		x = self.shift[0] + dx
		y = self.shift[1] + dy
		self.shift = (x, y)

	# Zooms in or out depending on the mouse motion
	# - "new_pos" is current screen position of mouse
	def scaling_canvas(self, new_pos):
		# Calculate new and last distance from top left corner of the canvas
		old_distance = self.distance(self.last_pos, self.shift)
		new_distance = self.distance(new_pos, self.shift)

		# Prevent dividing by zero
		if old_distance > 0:
			# Calculate new zoom value
			ratio = new_distance / old_distance
			self.zoom *= ratio

	# Functions handling mouse clicks =========================================
	
	# This function is called when mouse click occurs
	# - "widget" is the Window (not used)
	# - "event" carries information about click (not used)
	def on_mouse_click(self, widget, event):
		# Get current mouse position on screen
		screen_pos = self.get_pointer()

		# Get mouse position on canvas
		canvas_pos = self.screen_to_canvas_pos(screen_pos)

		if self.mode == "color_dialog":
			self.color_dialog_click(screen_pos, canvas_pos)

		# Switch some mode to "edit" or "object" mode when click occurs
		elif self.mode == "moving_objects":
			self.save_stage()
			self.mode = "object"

		elif self.mode == "moving_points":
			self.save_stage()
			self.mode = "edit"

		elif self.mode == "rotating_points":
			self.save_stage()
			self.mode = "edit"

		elif self.mode == "moving_bezier_control_point":
			self.save_stage()
			self.mode = "edit"

		elif self.mode == "scaling_points":
			self.save_stage()
			self.mode = "edit"

		elif self.mode == "moving_canvas":
			self.save_stage()
			self.mode = "object"

		elif self.mode == "scaling_canvas":
			self.save_stage()
			self.mode = "object"

		# Select object(s)
		elif self.mode == "object":
			self.select_object(canvas_pos)

		# Select point(s) or edge(s)
		elif self.mode == "edit":
			self.select_part(canvas_pos)

		# Return True in order to correctly handle the event
		return True

	# This function is called when mouse button is released
	# - "widget" is the Window (not used)
	# - "event" carries information about click (not used)
	def on_mouse_release(self, widget, event):
		# Stop changing color when mouse button is no longer pressed
		if self.mode == "changing_hue":
			self.save_stage()
			self.mode = "color_dialog"
		
		elif self.mode == "changing_saturation":
			self.save_stage()
			self.mode = "color_dialog"
		
		elif self.mode == "changing_value":
			self.save_stage()
			self.mode = "color_dialog"
		
		elif self.mode == "changing_alpha":
			self.save_stage()
			self.mode = "color_dialog"
		
		# End changing dimensions of selction box
		elif self.mode == "selecting_box":
			self.selection_box_done()

		# Return True in order to correctly handle the event
		return True

	# This function is called when selection box is created. It calls function
	# which will select all points within it.
	def selection_box_done(self):
		# Select object
		index = self.active_objects[0]
		object = self.all_objects[index]

		# Get start and end point of the selection box
		a = self.selection_box_start
		b = self.selection_box_end

		# Select points within the selection box
		object.selection_box(a, b)

		# Switxch mode back to "edit"
		self.mode = "edit"

	# Handles mouse click when color dialog is opened
	# - "screen_pos" is mouse position on the screen
	# - "canvas_pos" is mouse position on the canvas
	def color_dialog_click(self, screen_pos, canvas_pos):
		# If mouse is over the color dialog, check if mouse is also over cursor
		# of some of its color bar. If so, turn the right mode on, which will
		# enable dragging the cursor and changing the color
		if self.color_dialog.mouse_over(screen_pos):
			if self.color_dialog.on_cursor(screen_pos) == "hue":
				self.mode = "changing_hue"
			elif self.color_dialog.on_cursor(screen_pos) == "saturation":
				self.mode = "changing_saturation"
			elif self.color_dialog.on_cursor(screen_pos) == "value":
				self.mode = "changing_value"
			elif self.color_dialog.on_cursor(screen_pos) == "alpha":
				self.mode = "changing_alpha"
		
		# Pick color from image
		else:
			# Select initial color to None
			color = None

			# For all objects from forground to background
			for i in range(len(self.all_objects)-1, -1, -1):
				# Select object
				object = self.all_objects[i]

				# Get index of edge (of the object) under the mouse cursor.
				# Index will be None, if there is no edge beneath the mouse.
				index = object.mouse_over_edge(canvas_pos, self.zoom)

				# If mouse is over some edge
				if index != None:
					# Get color of the edge
					color = object.all_edges[index].color_real
					break

				# If mouse is over no edge (of the object)
				else:
					# Get index of face (of the object) under the mouse cursor.
					# Index will be None, if there is no face beneath the
					# mouse. 
					index = object.mouse_over_face(canvas_pos)

					# If mouse is over some face
					if index != None:
						# Get color of the face
						color = object.all_faces[index].color_inactive
						break
			
			# If there was some edge or face beneath the mouse cursor
			if color != None:
				# Get active object
				index = self.active_objects[0]
				object = self.all_objects[index]

				# Change color of the active object
				object.change_color(color)

	# Selects object, which is beneath the mouse cursor
	# - "canvas_pos" is mouse position on the canvas
	def select_object(self, canvas_pos):
		# For all objects from forground to background
		for i in range(len(self.all_objects)-1, -1, -1):
			# Get the object
			object = self.all_objects[i]

			# If mouse is over some point of "object" index of that point
			# is returned by "mouse_over_point" function. Otherwise "None"
			# is returned.
			point_index = object.mouse_over_point(canvas_pos, self.zoom)

			# If object was clicked
			if (object.mouse_over_edge(canvas_pos, self.zoom) != None or
				object.mouse_over_face(canvas_pos) != None or
				(point_index != None and not
					object.is_ending_point(point_index))):
				# If left Ctrl is held add index of the object to
				# list "active_objects" (indices, which already had been
				# there will stay in the list)
				if self.is_held(Gdk.KEY_Control_L):
					# If index "i" of the object "object" is already in
					# "ative_objects" list, this index is going to be
					# removed, while all other indices will stay
					if i in self.active_objects:
						self.active_objects.remove(i)

					# If index "i" of the object "object" is not in
					# "active_objects" list yet, it is going to be added,
					# while all other indices will stay
					else:
						self.active_objects.append(i)

				# If left Ctrl is not held all indices, which had been in
				# "active_objects" list, will be removed from the list and
				# replaced by index of the object
				else:
					self.active_objects = [i]

				# Because one object has been already (de)selected, looking
				# for another object sould be stopped
				return True
		
		# If mouse is not over any object all objects are going to be
		# deselected (if left Ctrl is not held)
		else:
			if not self.is_held(Gdk.KEY_Control_L):
				self.active_objects = []

	# Selects part of the active object, which is beneatch the mouse cursor
	# - "canvas_pos" is mouse position on the canvas
	def select_part(self, canvas_pos):
		# Mode "edit" can be activated only if only one object is active.
		# Therefore "active_object[0]" is index of the (only) active
		# object.
		index = self.active_objects[0]

		# Access to the object itself using "index"
		object = self.all_objects[index]

		# If Ctrl key is being held
		ctrl_held = self.is_held(Gdk.KEY_Control_L)

		# If mouse if over no point, edge or face
		if not object.select_part(canvas_pos, ctrl_held, self.zoom):
			# Start selection box mode
			self.selection_box_start = canvas_pos
			self.mode = "selecting_box"

	# Functions handling keyboard events ======================================

	# Function "on_key_press" is called when some key is pressed
	# - "widget" is the Window (not used)
	# - "event" carries information about key press (which key etc.)
	def on_key_press(self, widget, event):
		# Get ID of the pressed key
		key = event.keyval

		# Add ID of the pressed key to "held_keys" array if it is not already
		# there
		self.add_to_list(self.held_keys, key)
	
		# Check what key was pressed. Then call appropriate functions according
		# to current mode.
		if key == Gdk.KEY_n:
			if self.mode == "object":
				self.add_object()
			elif self.mode == "edit":
				self.add_point()
			elif self.mode == "welcome":
				self.file_chooser()

		elif key == Gdk.KEY_Escape:
			if self.mode == "object":
				self.back_to_welcome()

		elif key == Gdk.KEY_Tab:
			if self.mode == "object":
				self.activate_edit_mode()
			elif self.mode == "edit":
				self.activate_object_mode()

		elif key == Gdk.KEY_g:
			if self.mode == "object":
				self.move_objects()
			elif self.mode == "edit":
				self.move_points()

		elif key == Gdk.KEY_r:
			if self.mode == "edit":
				self.rotate_points()

		elif key == Gdk.KEY_a:
			if self.mode == "object":
				self.select_all_objects()
			elif self.mode == "edit":
				self.select_all_points()

		elif key == Gdk.KEY_l:
			if self.mode == "edit":
				self.connect_points()

		elif key == Gdk.KEY_f:
			if self.mode == "edit":
				self.create_face()

		elif key == Gdk.KEY_d:
			if self.mode == "object":
				self.delete_objects()
			elif self.mode == "edit":
				self.delete_part()

		elif key == Gdk.KEY_s:
			if self.mode == "object":
				self.save_project()
			if self.mode == "edit":
				self.mode = "scaling_points"

		elif key == Gdk.KEY_e:
			if self.mode == "edit":
				self.add_point_with_edges()

		elif key == Gdk.KEY_v:
			if self.mode == "edit":
				self.flip_points_vertically()

		elif key == Gdk.KEY_q:
			if self.mode == "edit":
				self.select_all_edges()
			elif self.mode == "object":
				self.add_circle()

		elif key == Gdk.KEY_w:
			if self.mode == "edit":
				self.select_all_faces()

		elif key == Gdk.KEY_c:
			if self.mode == "edit":
				self.show_color_dialog()
			elif self.mode == "color_dialog":
				self.mode = "edit"
			elif self.mode == "object":
				self.duplicate_objects()

		elif key == Gdk.KEY_k:
			if self.mode == "edit":
				self.create_bezier()

		elif key == Gdk.KEY_u:
			if self.mode == "object":
				self.order_bottom()

		elif key == Gdk.KEY_i:
			if self.mode == "object":
				self.order_down()
			elif self.mode == "edit":
				self.change_thickness()

		elif key == Gdk.KEY_o:
			if self.mode == "object":
				self.order_up()
			elif self.mode == "welcome":
				self.file_manager("open")

		elif key == Gdk.KEY_p:
			if self.mode == "object":
				self.order_top()

		elif key == Gdk.KEY_h:
			if self.mode == "edit":
				self.flip_points_horizontally()
			elif self.mode == "object":
				self.change_canvas_size()

		elif key == Gdk.KEY_t:
			if self.mode == "edit":
				self.add_point_with_beziers()

		elif key == Gdk.KEY_z:
			self.undo()

		elif key == Gdk.KEY_y:
			self.redo()

		elif key == Gdk.KEY_m:
			if self.mode == "object":
				self.mode = "moving_canvas"

		elif key == Gdk.KEY_j:
			if self.mode == "edit":
				self.switch_edge()
			elif self.mode == "object":
				self.mode = "scaling_canvas"
		
		elif key == Gdk.KEY_b:
			if self.mode == "object":
				self.export_png()
			

	# Function "on_key_press" is called when some key is released
	# - "widget" is the Window (not used)
	# - "event" carries information about key press (which key etc.)
	def on_key_release(self, widget, event):
		# Remove ID of the earlier pressed key from "held_keys" array. It is
		# possible, that the key had been held even before the app was started.
		# Then its ID would not be in the list, since ID is added when a key
		# is pressed. Because of this, it is needed to test if the ID is in the
		# list before it is going to be removed. 
		if event.keyval in self.held_keys:
			self.held_keys.remove(event.keyval)

	# Opens dialog for changing thickness of active edge(s)
	def change_thickness(self):
		# Get object
		index = self.active_objects[0]
		object = self.all_objects[index]
		
		# If some edges are active
		if len(object.active_edges) > 0:
			# Get average thickness
			average = 0
			for edge_index in object.active_edges:
				edge = object.all_edges[edge_index]
				average += edge.width
			average /= len(object.active_edges)

			# Create dialog for changing thickness
			dialog = DialogThickness(self, average)

			# Get the signal, which indicates what buttom was pressed
			response = dialog.run()

			# If "Open" button was pressed
			if response == Gtk.ResponseType.OK:
				print("The OK button was clicked")

				# For all active edges
				for edge_index in object.active_edges:
					# Set thickness of edge to new value
					edge = object.all_edges[edge_index]
					edge.width = dialog.get_thickness()

				# Delete the dialog from the memory
				dialog.destroy()

			# If "Open" button was pressed
			elif response == Gtk.ResponseType.CANCEL:
				print("The Cancel button was clicked")
				
				# Delete the dialog from the memory
				dialog.destroy()			

	# Opens dialog for changing canvas size
	def change_canvas_size(self):
		# Create dialog for changing canvas size
		dialog = DialogCanvasSize(self, self.width, self.height)

		# Get the signal, which indicates what buttom was pressed
		response = dialog.run()

		# If "Open" button was pressed
		if response == Gtk.ResponseType.OK:
			print("The OK button was clicked")

			# Set new dimensions
			self.width = dialog.get_width()
			self.height = dialog.get_height()

			# Delete the dialog from the memory
			dialog.destroy()
		elif response == Gtk.ResponseType.CANCEL:
			print("The Cancel button was clicked")

			# Delete the dialog from the memory
			dialog.destroy()

	# Changes line to Bezier curve and vice versa 
	def switch_edge(self):
		index = self.active_objects[0]
		object = self.all_objects[index]
		object.switch_edge()
		self.save_stage()

	# Exports image as png file. Note that there might be some differences
	# between image shown in the window and exported image (for example
	# single points will not be rendered).
	def export_png(self):
		# Create new surface to draw on
		surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(self.width),
			int(self.height))

		# Get context of the surface
		ctx = cairo.Context(surface)

		# Draw Background
		ctx.set_source_rgb(1, 1, 1)
		ctx.rectangle(0, 0, self.width, self.height)
		ctx.fill()

		# Settings
		ctx.set_miter_limit(cairo.LINE_JOIN_ROUND)
		ctx.set_line_cap(cairo.LINE_CAP_ROUND)

		# Draw all objects
		for i, object in enumerate(self.all_objects):
			object.draw(ctx, i in self.active_objects, self.mode, (0, 0), 1, False)

		# Create the file
		surface.write_to_png(self.project_name.split(".")[0] + ".png")

		print("Image has been exported")

	# Converts screen position to canvas position
	def screen_to_canvas_pos(self, pos):
		x = (pos[0] - self.shift[0]) / self.zoom
		y = (pos[1] - self.shift[1]) / self.zoom
		return (x, y)

	# Saves stage to the history list
	def save_stage(self):
		print("stage saved")

		# Increase history index
		self.history_index += 1

		# Delete all stages after current stage
		while self.history_index <= len(self.history) - 1:
			self.history.pop(-1)

		# Get copy of all objects
		all_objects = []
		for object in self.all_objects:
			object_copy = copy.deepcopy(object)
			all_objects.append(object_copy)

		# Get current properties
		mode = self.mode
		active_objects = self.active_objects
		shift = self.shift
		zoom = self.zoom
		width = self.width
		height = self.height

		# Create a tuple of all object and the mode
		stage = {"all_objects": all_objects,
				 "mode": mode,
				 "active_objects": active_objects,
				 "shift": shift,
				 "zoom": zoom,
				 "width": width,
				 "height": height}

		# Save copied objects to the history list
		self.history.append(stage)

	# Goes one step back in the histoy
	def undo(self):
		if self.history_index > 0:
			print("undo")

			self.history_index -= 1
			stage = self.history[self.history_index]

			all_objects = []
			for object in stage["all_objects"]:
				all_objects.append(object)
			self.all_objects = all_objects

			self.mode = stage["mode"]
			self.active_objects = stage["active_objects"]
			self.shift = stage["shift"]
			self.zoom = stage["zoom"]
			self.width = stage["width"]
			self.height = stage["height"]

	# Goes one step forward in the histoy
	def redo(self):
		if self.history_index < len(self.history) - 1:
			print("redo")
			self.history_index += 1
			stage = self.history[self.history_index]

			all_objects = []
			for object in stage["all_objects"]:
				all_objects.append(object)
			self.all_objects = all_objects

			self.mode = stage["mode"]
			self.active_objects = stage["active_objects"]
			self.shift = stage["shift"]
			self.zoom = stage["zoom"]
			self.width = stage["width"]
			self.height = stage["height"]

	# Flips all active points horizontally. Axis is vertical line, which goes
	# trough the object center
	def flip_points_horizontally(self):
		index = self.active_objects[0]
		object = self.all_objects[index]
		object.flip_points_horizontally()
		self.save_stage()

	# Flips all active points vertically. Axis is horizontal line, which goes
	# trough the object center
	def flip_points_vertically(self):
		index = self.active_objects[0]
		object = self.all_objects[index]
		object.flip_points_vertically()
		self.save_stage()

	# Sends active objects to the forground
	def order_top(self):
		# Get index, where should the most top of the active objects be sent
		goal_index = len(self.all_objects) - 1

		# For active objects from forground to background
		for index in sorted(self.active_objects, reverse=True):
			# Move object, which should be moved to the right level and move
			# all objects on the way one level down
			for i in range(index, goal_index):
				temp = self.all_objects[i+1]
				self.all_objects[i+1] = self.all_objects[i]
				self.all_objects[i] = temp

			# Change goal index for next object, which shall be moved
			goal_index -= 1

		# Moved objects are now on top, so start and end index of the interval
		# with recently moved objects can be got
		start = len(self.all_objects) - len(self.active_objects)
		end = len(self.all_objects)

		# Change indicies in list of active objects in such way, that same
		# objects will be still selected
		self.active_objects = list(range(start, end))

		self.save_stage()

	# Sends active objects to the background
	def order_bottom(self):
		# Get index, where should the most bottom of the active objects be sent
		goal_index = 0

		# For activ eobjects from background to forground
		for index in sorted(self.active_objects):
			# Move object, which should be moved to the right level and move
			# all objects on the way one level up
			for i in range(index, goal_index, -1):
				temp = self.all_objects[i-1]
				self.all_objects[i-1] = self.all_objects[i]
				self.all_objects[i] = temp

			# Change goal index for next object, which shall be moved
			goal_index += 1

		# Moved objects are now at the bottom, so it is possible to get end
		# index of all moved points (start index is 0)
		end = len(self.active_objects)

		# Change indicies in list of active objects in such way, that same
		# objects will be still selected
		self.active_objects = list(range(end))

		self.save_stage()

	# Sends active objects one level up
	def order_up(self):
		# Get top index
		top = len(self.all_objects) - 1

		# For all active objects from forground to background
		for index in sorted(self.active_objects, reverse=True):
			# If object can be moved
			if index < top:
				# Switch object, which should be moved with the object above it
				temp = self.all_objects[index+1]
				self.all_objects[index+1] = self.all_objects[index]
				self.all_objects[index] = temp

				# Change index in such way, that same object will stay active
				self.active_objects.remove(index)
				self.active_objects.append(index+1)
			
			# If object is on the top, top index should be decreased
			else:
				top -= 1

		self.save_stage()

	# Sends active objects one level down
	def order_down(self):
		# Get bottom index
		bottom = 0

		# For all active objects from background to forground
		for index in sorted(self.active_objects):
			# If object can be moved
			if index > bottom:
				# Switch object, which should be moved with the object beneath
				# it
				temp = self.all_objects[index-1]
				self.all_objects[index-1] = self.all_objects[index]
				self.all_objects[index] = temp

				# Change index in such way, that same object will stay active
				self.active_objects.remove(index)
				self.active_objects.append(index-1)
			
			# If object is at the bottom, top index should be increased
			else:
				bottom += 1
		self.save_stage()

	# Creates a copy of selected object(s) and makes new object(s) active
	def duplicate_objects(self):
		# List of new active indicies
		new_active_inidices = []

		# For every active object
		for index in self.active_objects:
			# Get object
			object = self.all_objects[index]

			# Create copy of the object
			new_object = copy.deepcopy(object)

			# Add the copy to "all_objects" list
			self.all_objects.append(new_object)

			# Get new index
			new_index = len(self.all_objects) - 1

			# Add the new index to list of all new indices
			new_active_inidices.append(new_index)

		# Set active objects
		self.active_objects = new_active_inidices

		self.save_stage()

	# Creates an aproximation of circle using 4 Bezier curves
	def add_circle(self):
		# Set radius of the circle
		radius = 100

		# Get canvas position of the mouse
		mouse_pos = self.get_pointer()
		mouse_canvas_pos = self.screen_to_canvas_pos(mouse_pos)

		# Get canvas coordinates of 4 points between which those Bezier curves
		# will be
		a = (mouse_canvas_pos[0] + radius, mouse_canvas_pos[1])
		b = (mouse_canvas_pos[0], mouse_canvas_pos[1] - radius)
		c = (mouse_canvas_pos[0] - radius, mouse_canvas_pos[1])
		d = (mouse_canvas_pos[0], mouse_canvas_pos[1] + radius)
		points = [a, b, c, d]

		# Create new object with the points
		new = Object(points)

		# Constant for best circle approximation (it is the relative position
		# of the control points)
		k = 0.55228

		# Create all 4 Bezier curves between those 4 points
		new.create_bezier(0, 1)
		new.create_bezier(1, 2)
		new.create_bezier(2, 3)
		new.create_bezier(3, 0)

		# Set control points of the Bezier curves in such way, that it
		# approximates a circle as good as possible
		edge = new.all_edges[0]
		edge.c.pos = (0, - radius*k)
		edge.d.pos = (radius*k, 0)

		edge = new.all_edges[1]
		edge.c.pos = (- radius*k, 0)
		edge.d.pos = (0, - radius*k)

		edge = new.all_edges[2]
		edge.c.pos = (0, radius*k)
		edge.d.pos = (- radius*k, 0)

		edge = new.all_edges[3]
		edge.c.pos = (radius*k, 0)
		edge.d.pos = (0, radius*k)

		# Deactivate all (actually only the last one) edges
		new.active_edges = []

		# Add new object to the list of all objects
		self.all_objects.append(new)

		# Make the object active
		index = len(self.all_objects) - 1
		self.active_objects = [index]

	# Enables "color_dialog" mode if some edge/face is selected
	def show_color_dialog(self):
		# Get the active object
		index = self.active_objects[0]
		object = self.all_objects[index]
		
		# If some face is selected
		if len(object.active_faces) > 0:
			pos = self.get_pointer()
			self.color_dialog.pos = self.get_pointer()
			self.mode = "color_dialog"

		# If some edge is selected
		if len(object.active_edges) > 0:
			self.color_dialog.pos = self.get_pointer()
			self.mode = "color_dialog"

	# Conectes exactly two selected points with Bezier curve
	def create_bezier(self):
		index = self.active_objects[0]
		object = self.all_objects[index]
		object.connect_points_with_bezier()
		self.save_stage()

	# Selects all edges, or deselects all edges, if all had been selected
	def select_all_edges(self):
		index = self.active_objects[0]
		object = self.all_objects[index]
		object.select_all_edges()

	# Selects all faces, or deselects all faces, if all had been selected
	def select_all_faces(self):
		index = self.active_objects[0]
		object = self.all_objects[index]
		object.select_all_faces()

	# Add a new point and connects it automatically using line with all points,
	# which had been active
	def add_point_with_edges(self):
		# Get canvas position of the mouse
		pos = self.get_pointer()
		pos = self.screen_to_canvas_pos(pos)

		# Get the active object
		index = self.active_objects[0]
		object = self.all_objects[index]

		# If some points had been active
		if len(object.active_points) > 0:
			self.mode = "moving_points"
			object.add_point_with_edges(pos)
			self.save_stage()
		
		# If no points had been active
		else:
			print("Point with edge(s) cannot be added")

	# Add a new point and connects it automatically using Bezier curve with all
	# points, which had been active
	def add_point_with_beziers(self):
		# Get canvas position of the mouse
		pos = self.get_pointer()
		pos = self.screen_to_canvas_pos(pos)
		
		# Get the active object
		index = self.active_objects[0]
		object = self.all_objects[index]

		# If some points had been active
		if len(object.active_points) > 0:
			self.mode = "moving_points"
			object.add_point_with_beziers(pos)
			self.save_stage()

		# If no points had been active
		else:
			print("Point with Bezier curve(s) cannot be added")

	# Quits the project and returns to the welcome screen
	def back_to_welcome(self):
		self.save_project()
		self.mode = "welcome"

	# This function opens project. If the project already exists, it will load
	# it. Otherwise it will create it from the scratch.
	def open_project(self, name):
		# If file "name" already exists, open it
		if os.path.isfile(name):
			with open(name, "rb") as input:
				# Load the object "Window" from the saved file
				loaded = pickle.load(input)

				# Save all needed properties from the loaded object to current
				# "Window" object
				self.all_objects = loaded.all_objects
				self.shift = loaded.shift
				self.zoom = loaded.zoom
				self.history = loaded.history
				self.width = loaded.width
				self.height = loaded.height
		
		# If file "name" does not exist, initialize it
		else:
			# Set list of all objects empty
			self.all_objects = []

			# Set initial canvas size as equal to the screen size
			screen_size = self.get_size()
			self.width = screen_size[0]
			self.height = screen_size[1]

			# Create the file, so later information can be saved into it
			f = open(name, "wb")
			f.close()

		# Set project name (including the extension)
		self.project_name = name

		# File has been loaded, so "welcome" mode can be exited
		self.mode = "object"

		print("Project has been succesfully loaded")

	# Saves the project
	def save_project(self):
		with open(self.project_name, "wb") as output:
			pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)
		print("Project was saved")

	# Deletes active part of the active object object, whether it is point,
	# edge or face
	def delete_part(self):
		index = self.active_objects[0]
		object = self.all_objects[index]
		object.delete_part()
		self.save_stage()

	# This function deletes all active objects. List "active_objects" is then
	# set empty.
	def delete_objects(self):
		# For all indices of sorted list "active_objects". It is important that
		# the list is sorted in reverse order. The reason for that is, that
		# when an object with some index is removed from "all_objects" list all
		# objects, which are (in the list) after the deleted object, are going
		# to have their real indices (in the list) decreased by one, but
		# indices stored in "active_objects" would not be updated. One solution
		# would be to update indices in "active_objects" everytime an object is
		# deleted. It would be possible, but this is more effective and simple.
		for index in sorted(self.active_objects, reverse=True):
			# Remove object with index "index" from "all_objects" list
			self.all_objects.pop(index)

		# Deactivate all objects
		self.active_objects = []

		self.save_stage()

	# Function "activate_object_mode" changes mode from various modes to
	# "object" mode
	def activate_object_mode(self):
		self.mode = "object"
		self.print_mode()

	# Function "move_objects" changes mode from "object" to "moving objects"
	# mode
	def move_objects(self):
		if len(self.active_objects) > 0:
			self.mode = "moving_objects"
			self.print_mode()

	# Function "move_points" changes mode from "edit_mode" to "moving_points"
	def move_points(self):
		index = self.active_objects[0]
		object = self.all_objects[index]

		# Move points
		if len(object.active_points) > 0:
			self.mode = "moving_points"
			self.print_mode()

		# Move Bezier control point
		elif object.active_bezier_control_point != None:
			self.mode = "moving_bezier_control_point"

	# Function "rotate_points" changes mode from "edit" to "rotating_points"
	def rotate_points(self):
		index = self.active_objects[0]
		object = self.all_objects[index]

		if len(object.active_points) > 0:
			self.mode = "rotating_points"
			self.print_mode()

	# Creates face if active edge can form a perfect loop
	def create_face(self):
		# Active object
		index = self.active_objects[0]
		object = self.all_objects[index]
		object.create_face()
		self.save_stage()

	# Function "activate_edit_mode" changes mode from "object" to "edit" mode,
	# but only if just one object is active
	def activate_edit_mode(self):
		if len(self.active_objects) == 1:
			self.mode = "edit"
			self.print_mode()

	# Function "connect_points" is called when line (segment) between two points
	# shall be created. This function can be called only while in "edit" mode.
	# It passes the instruction to the object. 
	def connect_points(self):
		# Since "edit" mode is active, only one object is active. Therefore
		# "active_objects[0]" is index of the only active object.
		index = self.active_objects[0]

		# Access to the active object using "index"
		object = self.all_objects[index]

		# Create line (segment) if exactly two points are selected
		object.connect_points()

		self.save_stage()

	# Function "select_all_points" selects all points of the active object. It
	# can be called only when "edit" mode is active.
	def select_all_points(self):
		# Since "edit" mode is active, only one object is active. Therefore
		# "active_objects[0]" is index of the only active object.
		index = self.active_objects[0]
		
		# Access to the active object using "index"
		object = self.all_objects[index]

		# If some points (of the active object) had not been active, select all
		# points (of the active object). Otherwise deselect all points (of the
		# active object).
		object.select_all_points()

	# Function "select_all_objects" selects all objects if at least on of all
	# objects had not been selected. Otherwise it deselects all objects.
	def select_all_objects(self):
		# How many objects are in the scene
		nu_objects = len(self.all_objects)

		# If "active_objects" list has as many indicies as "all_objects" list,
		# then are all objects selected. Therefore all objects shall be
		# deselected by setting "active_objects" list empty.
		if len(self.active_objects) == nu_objects:
			self.active_objects = []

		# If at least one object had not been selected, then select all objects
		# by filling "active_objects" with indices of all objects.
		else:
			self.active_objects = list(range(nu_objects))

	# Function "add_object" adds a new object and turns "moving_objects" mode.
	# Because of the mode, the new object can be moved by mouse, until a mouse
	# click occurs. The new object is automatically set as the only active
	# object.
	def add_object(self):
		# Set mode
		self.mode = "moving_objects"

		# Create a list of all coordiantes of all object's points. So far a new
		# object contains always only a single point with same absolute
		# coordinates as the mouse cursor.
		pos = self.get_pointer()
		pos = self.screen_to_canvas_pos(pos)
		points = [pos]

		# Create a new object. Note that position of the object center will be
		# automatically calculated as a average (or center of mass) of all
		# points present in the "points" list.
		new = Object(points)

		# Add new object to "all_objects" list
		self.all_objects.append(new)

		# Add index of the new object to "active_objects" list and remove all
		# other indices
		self.active_objects = [len(self.all_objects) - 1]

	# Function "add_point" adds a new point to the active object. This function
	# can be called only from "edit" mode. The point is automatically set as an
	# only active point, which means that point can be moved until a mouse
	# click occurs, because "moving_points" mode is on
	def add_point(self):
		# Set mode
		self.mode = "moving_points"

		# Get current mouse position (x, y). It will be initial position of the
		# new point.
		pos = self.get_pointer()
		pos = self.screen_to_canvas_pos(pos)

		# Since "edit" mode had been active, only one object is active.
		# Therefore "active_objects[0]" is index of the active object.
		index = self.active_objects[0]

		# Access to the active object using "index"
		object = self.all_objects[index]

		# Add point to object's "all_points" list and set the new point as the
		# only active point (of the active object)
		object.add_point(pos)

	# Little helper functions =================================================

	# Opens file chooser dialog
	def file_chooser(self):
		# Create file chooser dialog
		dialog = Gtk.FileChooserDialog("Please choose a file", self,
			Gtk.FileChooserAction.SAVE,
			(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
			 Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
		
		# Create file filter, so only folders and file with 'tvm' extension
		# will be displayed
		tvm_filter = Gtk.FileFilter()
		tvm_filter.set_name("Tivom files")
		tvm_filter.add_pattern("*.tvm")

		# Add the file filter to the dialog
		dialog.add_filter(tvm_filter)
		
		# Get the signal, which indicates what buttom was pressed
		response = dialog.run()

		# If "Open" button was pressed
		if response == Gtk.ResponseType.OK:
			# Get absolute path (and file name)
			full_name = dialog.get_filename()

			# Split "full_name" to name itself and the extension 
			name_and_extension = full_name.split(".")

			# If there is no dot in "full_name" which means, that there is no
			# extension.
			if len(name_and_extension) == 1:
				# Add the extension to the name
				full_name += ".tvm"

				# Open project
				self.open_project(full_name)

			# If there is dot, which means, that there is an extension
			elif len(name_and_extension) == 2:
				# If the extension is not correct
				if name_and_extension[1] != "tvm":
					print("Project will not be loaded (incorrect extension)")
				
				# If file has 'tvm' extension
				else:
					# Open project
					self.open_project(full_name)
			else:
				print("Project will not be loaded (invalid file format)")

		# If "Cancel" button was pressed
		elif response == Gtk.ResponseType.CANCEL:
			print("Project will not be loaded (cancel button was clicked)")

		# Delete the dialog from the memory
		dialog.destroy()

	# Closes the window
	# - "widget" is the Window (not used)
	# - "event" carries information about click (not used)
	def quit(self, widget, event):
		# If some project is opened
		if self.mode != "welcome":
			# Save the project
			self.save_project()

		print("Window is going to be closed")

		# Close the window
		Gtk.main_quit()

	# Function "print_mode" prints current mode
	def print_mode(self):
		print("Mode \"{}\" has been activated".format(self.mode))

	# Function "is_held" is a little helper function, which helps to make code
	# a bit more clean. It returns "True" if key with ID "key" is being held.
	# - "key" is ID of the key, which is being checked
	def is_held(self, key):
		if key in self.held_keys:
			return True
		else:
			return False

	# Function "add_to_list" is a little helper function, which adds value to
	# the array if it is not already in it. If the value already is in the list
	# False is returned.
	# - "array" is thne array to which the value is going to be added
	# - "value" is the value, which is going to be added to the array
	def add_to_list(self, array, value):
		if value in array:
			return False
		else:
			array.append(value)

	# Function "distance" is a little helper function, which calculates
	# distance between two points
	# - "a" is position of the first point
	# - "b" is position of the second point
	def distance(self, a, b):
		x = abs(a[0] - b[0])
		y = abs(a[1] - b[1])
		return math.sqrt(x**2 + y**2)


# =============================================================================
# This is a Gtk dialog for changing thickness of edges
# =============================================================================
class DialogThickness(Gtk.Dialog):
	# Creates and displays the dialog
	# - "parent" is is the Window widget (not used)
	# - "thickness" is initial thickness
	def __init__(self, parent, thickness):
		# Create the dialog itself
		Gtk.Dialog.__init__(self, "Change edge thickness", parent, 0,
			(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
			Gtk.STOCK_OK, Gtk.ResponseType.OK))

		# Set its size
		self.set_default_size(120, 60)

		# Create the input with plus and minus buttons
		adjustment = Gtk.Adjustment(thickness, 0, 1000, 1, 1, 0)
		self.spinbutton = Gtk.SpinButton()
		self.spinbutton.set_adjustment(adjustment)

		# Add the input (with plus and minus) buttons to layout
		hbox = Gtk.Box(orientation=1)
		hbox.pack_start(self.spinbutton, False, False, 0)
		box = self.get_content_area()
		box.add(hbox)

		# Display it
		self.show_all()

	# Returns the value from the dialog
	def get_thickness(self):
		return self.spinbutton.get_value()


# =============================================================================
# This is a Gtk dialog for changing canvas size
# =============================================================================
class DialogCanvasSize(Gtk.Dialog):
	# Creates and displays the dialog
	# - "parent" is is the Window widget (not used)
	# - "width" is initial width
	# - "height" is initial height
	def __init__(self, parent, width, height):
		# Create the dialog itself
		Gtk.Dialog.__init__(self, "Change canvas size", parent, 0,
			(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
			Gtk.STOCK_OK, Gtk.ResponseType.OK))

		# Set its size
		self.set_default_size(150, 100)

		# Create the input with plus and minus buttons for width
		adjustment = Gtk.Adjustment(width, 1, 10000, 100, 100, 0)
		self.spinbutton = Gtk.SpinButton()
		self.spinbutton.set_adjustment(adjustment)

		# Create the input with plus and minus buttons for height
		adjustment = Gtk.Adjustment(height, 1, 10000, 100, 100, 0)
		self.spinbutton2 = Gtk.SpinButton()
		self.spinbutton2.set_adjustment(adjustment)

		# Add the inputs (with plus and minus) buttons to layout
		hbox = Gtk.Box(orientation=1)
		hbox.pack_start(self.spinbutton, False, False, 0)
		hbox.pack_start(self.spinbutton2, False, False, 0)
		box = self.get_content_area()
		box.add(hbox)

		# Display it
		self.show_all()

	# Returns width
	def get_width(self):
		return self.spinbutton.get_value()

	# Returns height
	def get_height(self):
		return self.spinbutton2.get_value()


# =============================================================================
# This is a class for color bar, which is used for changing hue, saturation,
# value and alpha in the color dialog
# =============================================================================
class ColorBar():
	def __init__(self, x, y):
		# Set size
		self.width = 200
		self.height = 20

		# Set position relative to the color dialog
		self.x = x
		self.y = y

	# Draws the color bar itself and its cursor
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	# - "pos" is screen position of top left corner of the color dialog
	# - "h" is current hue (0 to 1)
	# - "s" is current saturation (0 to 1)
	# - "v" is current value (0 to 1)
	# - "a" is current alpha (0 to 1)
	# - "gradient" is which color channel should create the gradient (0 - h,
	#    1 - s, 2 - v, 3 - a)
	def draw(self, ctx, pos, h, s, v, a, gradient):
		# Calculate screen position
		x = pos[0] + self.x
		y = pos[1] + self.y

		# Set parameter to zero
		p = 0

		# Set change of parameter
		dp = 0.1

		# Create a linear gradient
		lg = cairo.LinearGradient(x, 0, x+self.width, 0)
		
		# Add color stops to the linear gradient. Color depends on the paramter
		while p <= 1:
			# If this is a color bar for hue
			if gradient == 0:
				rgb = colorsys.hsv_to_rgb(p, s, v)
				lg.add_color_stop_rgba(p, rgb[0], rgb[1], rgb[2], 1)
			
			# If this is a color bar for saturation
			elif gradient == 1:
				rgb = colorsys.hsv_to_rgb(h, p, v)
				lg.add_color_stop_rgba(p, rgb[0], rgb[1], rgb[2], 1)
			
			# If this is a color bar for value
			elif gradient == 2:
				rgb = colorsys.hsv_to_rgb(h, s, p)
				lg.add_color_stop_rgba(p, rgb[0], rgb[1], rgb[2], 1)
			
			# If this is a color bar for value
			elif gradient == 3:
				rgb = colorsys.hsv_to_rgb(h, s, v)
				lg.add_color_stop_rgba(p, rgb[0], rgb[1], rgb[2], p)
			
			# Update the parameter
			p += dp

		# Draw the color bar itself
		ctx.rectangle(x, y, self.width, self.height)
		ctx.set_source(lg)
		ctx.fill()

		# Get horizontal screen position of the cursor
		if gradient == 0:
			x = x + self.width * h
		elif gradient == 1:
			x = x + self.width * s
		elif gradient == 2:
			x = x + self.width * v
		elif gradient == 3:
			x = x + self.width * a

		# Save the cursor position
		self.cursor_x = x

		# Draw black half of the cursor
		ctx.set_source_rgba(0, 0, 0, 1)
		ctx.move_to(x - 2, y)
		ctx.line_to(x + 2, y)
		ctx.line_to(x, y + 5)
		ctx.line_to(x - 2, y)
		ctx.fill()

		# Draw white half of the cursor
		ctx.set_source_rgba(1, 1, 1, 1)
		ctx.move_to(x - 2, y + self.height)
		ctx.line_to(x + 2, y + self.height)
		ctx.line_to(x, y + self.height - 5)
		ctx.line_to(x - 2, y + self.height)
		ctx.fill()

	# Checks if mouse is over the cursor of the color bar
	# - "color_dialog_pos" is screen position of the color dialog
	# - "mouse_pos" is screen position of the mouse
	def on_cursor(self, color_dialog_pos, mouse_pos):
		# Set threshold so the click does not have to be precise
		threshold = 7

		# Get screen position of the cursor
		x = self.cursor_x
		y = self.y + color_dialog_pos[1]

		# Check if mouse is in range from all directions
		left = x - threshold <= mouse_pos[0]
		right = x + threshold >= mouse_pos[0]
		top = y <= mouse_pos[1]
		bottom = y + self.height >= mouse_pos[1]

		# If mouse is over the cursor return True, otherwise return False
		if left and right and top and bottom:
			return True
		else:
			return False

	# Return new value depending on new mouse position (and position of the
	# color dialog)
	# - "mouse_x" is the horizontal screen position of the mouse
	# - "color_dialog_x" is the horizontal screen position of the color dialog
	def change(self, mouse_x, color_dialog_x):
		# If according to the mouse position the value should be less than 0
		if mouse_x < self.x + color_dialog_x:
			return 0
		
		# If according to the mouse position the value should be mode than 1
		elif mouse_x > self.x + color_dialog_x + self.width:
			return 1

		# If according to the mouse position the value is in range [0, 1]
		else:
			return (mouse_x - (color_dialog_x + self.x)) / self.width


# =============================================================================
# This is a dialog for changing color of edges and faces
# =============================================================================
class ColorDialog():
	def __init__(self):
		# Set size
		self.width = 204
		self.height = 90

		# Set backgroudn color
		self.background = (0.2, 0.2, 0.2, 1)

		# Create all color bars
		self.hue = ColorBar(2, 2)
		self.saturation = ColorBar(2, 4 + self.hue.height)
		self.value = ColorBar(2, 6 + 2*self.hue.height)
		self.alpha = ColorBar(2, 8 + 3*self.hue.height)

	# Draws the color dialog with all color bars on it
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	# - "rgba" is current color, which can me modified using color bars
	def draw(self, ctx, rgba):
		# Background
		ctx.set_source_rgba(*self.background)
		ctx.rectangle(self.pos[0], self.pos[1], self.width, self.height)
		ctx.fill()

		# Get alpha
		a = rgba[-1]

		# Get hsv from rgb
		hsv = colorsys.rgb_to_hsv(*rgba[:-1])

		# Draw all color bars
		self.hue.draw(ctx, self.pos, hsv[0], hsv[1], hsv[2], a, 0)
		self.saturation.draw(ctx, self.pos, hsv[0], hsv[1], hsv[2], a, 1)
		self.value.draw(ctx, self.pos, hsv[0], hsv[1], hsv[2], a, 2)
		self.alpha.draw(ctx, self.pos, hsv[0], hsv[1], hsv[2], a, 3)

	# Check if mouse is over the color dialog
	# - "mouse_pos" is screen position of mouse
	def mouse_over(self, mouse_pos):
		# Check if mouse is in the range from all directions
		left = self.pos[0] <= mouse_pos[0]
		right = self.pos[0] + self.width >= mouse_pos[0]
		top = self.pos[1] <= mouse_pos[1]
		bottom = self.pos[1] + self.height >= mouse_pos[1]

		# If mouse is over the color dialog return True, otherwise return False
		if left and right and top and bottom:
			return True
		else:
			return False

	# Check if mouse is over cursor of come color bar. Returns type of color
	# bar, if mouse is over some cursor, or None if mouse is not over any
	# cursor
	# - "mouse_pos" is screen position of mouse
	def on_cursor(self, mouse_pos):
		if self.hue.on_cursor(self.pos, mouse_pos):
			return "hue"
		elif self.saturation.on_cursor(self.pos, mouse_pos):
			return "saturation"
		elif self.value.on_cursor(self.pos, mouse_pos):
			return "value"
		elif self.alpha.on_cursor(self.pos, mouse_pos):
			return "alpha"
		else:
			return None

	# Get value of hue accroding to horizontal position of the mouse
	# - "mouse_x" is horizontal screen position of the mouse
	def change_hue(self, mouse_x):
		return self.hue.change(mouse_x, self.pos[0])

	# Get value of saturation accroding to horizontal position of the mouse
	# - "mouse_x" is horizontal screen position of the mouse
	def change_saturation(self, mouse_x):
		return self.saturation.change(mouse_x, self.pos[0])

	# Get value of value accroding to horizontal position of the mouse
	# - "mouse_x" is horizontal screen position of the mouse
	def change_value(self, mouse_x):
		return self.value.change(mouse_x, self.pos[0])

	# Get value of alpha accroding to horizontal position of the mouse
	# - "mouse_x" is horizontal screen position of the mouse
	def change_alpha(self, mouse_x):
		return self.alpha.change(mouse_x, self.pos[0])


# =============================================================================
# Class "Object" represents an object, which can contain points, edges, faces
# and other properties. This class also contains many helper functions,
# which test some property of the object and decides what shall be done.
# =============================================================================
class Object():
	# This function sets initial values and graphics constants
	# - "points" is a list of tuples (x, y), which represent all initial points
	#   of the new object
	def __init__(self, points):
		# Calculate the center of this object
		self.center = self.calculate_center(points)

		# Calculate list "all_points" of Point objects. All of them will
		# automatically calculate their position relative to the object center.
		self.all_points = [Point(pos, self.center) for pos in points]

		# Set new points as active
		self.active_points = [0]

		# List "all_edges" will contain all object's edges
		self.all_edges = []

		# List "active_edges" will contain indices (to "all_edges") of all
		# object's active edges
		self.active_edges = []

		# List "all_faces" will contain all object's faces
		self.all_faces = []

		# List "active_faces" will contain indicies (to "all_faces") of all
		# object's active faces
		self.active_faces = []

		self.active_bezier_control_point = None

		# Length of arms of the cross of the object center. It is used only for
		# drawing.
		self.center_radius = 10

		# Color of the object center. It is only used while in "edit" mode or
		# other edit-based mode.
		self.center_color = (0, 1, 0, 1)

		# Width of arms of the cross of the object center. It is used only for
		# drawing.
		self.center_width = 2

	# This function draw the whole object. That includes all points, edges,
	# faces and the object center. There is order in which different parts of
	# the object shall be drawn. At the background are drawn all faces, then
	# edges, points and at the foreground is the object center.
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	# - "active" is a boolean parameter which says if the object is active
	# - "mode" is current mode ("object", "edit" etc.)
	# - "shift" is screen position of top left canvas corner
	# - "zoom" is current canvas zoom
	# - "draw_non_real" is a boolean parameter, which says if helper lines
	#   should be drawn (for example when exporting, they should not be drawn)
	# TODO: non-real edges should be always on top those real ones
	def draw(self, ctx, active, mode, shift, zoom, draw_non_real):
		# Draw faces

		# List of edges, which are going to be highlighted, because they are
		# border of an active face
		highlighted_edges = []

		# For all faces
		for i, face in enumerate(self.all_faces):
			# If face is active and right mode is on, it will be shown by
			# highlighting edges of the active face. "new_hightlighed_edges"
			# is a list of edges of this face, if the face is active. If it is
			# not active, empty list is returned.
			new_highlighted_edges = self.draw_face(ctx, face, mode, active,
				shift, zoom, i)
			
			# Add edges from "new_hightlighed_edges" to "highlighted_edges" if
			# there are not already there
			for edge_index in new_highlighted_edges:
				if not edge_index in highlighted_edges:
					highlighted_edges.append(edge_index)

		# Draw edges
		for i, edge in enumerate(self.all_edges):
			face_edge = self.is_edge_of_face(i)
			self.draw_edge(ctx, edge, i, mode, active, i in highlighted_edges,
				shift, zoom, draw_non_real, face_edge)

		# Draw all points
		for i, point in enumerate(self.all_points):
			# Test if the point is an end point of some edge
			end_point = self.is_ending_point(i)

			# If point should be displayed
			if ((not end_point or (active and (
				mode == "edit" or
				mode == "changing_hue" or
				mode == "changing_saturation" or
				mode == "changing_value" or
				mode == "changing_alpha" or
				mode == "moving_points" or
				mode == "rotating_points" or
				mode == "moving_bezier_control_point" or
				mode == "scaling_points" or
				mode == "selecting_box"))) and draw_non_real):

				is_point_active = i in self.active_points
				point.draw(ctx, active, mode, is_point_active,
					self.center, shift, zoom)

		# Draw the object center if the object is active
		if active and draw_non_real:
			self.draw_center(ctx, shift, zoom)
	
	# Draws face
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	# - "face" is the face, which is going to be drawn
	# - "mode" is current mode ("object", "edit" etc.)
	# - "active" is a boolean parameter which says if the object is active
	# - "shift" is screen position of top left canvas corner
	# - "zoom" is current canvas zoom
	# - "index" is index of the face
	def draw_face(self, ctx, face, mode, active, shift, zoom, index):
		# Get and set face color
		color = face.color_inactive
		ctx.set_source_rgba(*color)

		# Get index of the first edge of the face
		first_edge_index = face.edges[0][0]

		# Get direction (True if the edge is from a to b, False if the edge is
		# from b to a) of the first edge of the face
		first_direction = face.edges[0][1]

		# Get the first edge itself
		first_edge = self.all_edges[first_edge_index]

		# Set index of the start point
		if first_direction:
			start_index = first_edge.a
		else:
			start_index = first_edge.b

		# Get start point itself
		start_point = self.all_points[start_index]		

		# Get screen position of the start point
		absolute_start_pos = self.absolute(start_point.pos, self.center)
		absolute_start_pos = self.screen_pos(absolute_start_pos, shift, zoom)

		# Set position of the first point also as position of the last point
		last_pos = absolute_start_pos

		# Draw face itself. Because of Cairo's antialiasing imperfection, it is
		# neccesary to draw face itself without antialiasing and then draw over
		# it just the border with antialiasing. If this is not done, there will
		# be thin line between two faces, which are just beside each other.
		
		# Turn off antialiasing for drawing face itself
		ctx.set_antialias(cairo.ANTIALIAS_NONE)

		# Move Cairo's cursor to the start point
		ctx.move_to(*absolute_start_pos)

		# For all face's edges
		for edge_index, a_to_b in face.edges:
			# Get edge
			edge = self.all_edges[edge_index]

			# Set index of the end point of this edge
			if a_to_b:
				end_index = edge.b
			else:
				end_index = edge.a

			# Get end point of the edge
			end = self.all_points[end_index]

			# Get screen position of the end point
			absolute_pos = self.absolute(end.pos, self.center)
			absolute_pos = self.screen_pos(absolute_pos, shift, zoom)

			# If edge is a line, draw line
			if edge.type == "line":
				ctx.line_to(*absolute_pos)

			# If edge is a Bezier curve, draw Bezier curve
			elif edge.type == "bezier":
				# If a is a start point
				if a_to_b:
					# Get screen position of the first Bezier control point
					# relative to the first point
					x = edge.c.pos[0] * zoom
					y = edge.c.pos[1] * zoom

					# Calculate screen position of the first Bezier control
					# point
					bcp1 = self.absolute((x, y), last_pos)

					# Get screen position of the second Bezier control point
					# relative to the second point
					x = edge.d.pos[0] * zoom
					y = edge.d.pos[1] * zoom

					# Calculate screen position of the second Bezier control
					# point
					bcp2 = self.absolute((x, y), absolute_pos)
				
				# If b is a start point
				else:
					# Get screen position of the first Bezier control point
					# relative to the first point
					x = edge.d.pos[0] * zoom
					y = edge.d.pos[1] * zoom

					# Calculate screen position of the first Bezier control
					# point
					bcp1 = self.absolute((x, y), last_pos)

					# Get screen position of the second Bezier control point
					# relative to the second point
					x = edge.c.pos[0] * zoom
					y = edge.c.pos[1] * zoom

					# Calculate screen position of the second Bezier control
					# point
					bcp2 = self.absolute((x, y), absolute_pos)

				# Get screen x and y coordinates of the end point
				x = absolute_pos[0]
				y = absolute_pos[1]

				# Define edge of the face
				ctx.curve_to(bcp1[0], bcp1[1], bcp2[0], bcp2[1], x, y)

			# Set screen position of the end point as last position
			last_pos = absolute_pos

		# Fill the face
		ctx.fill()

		# This is the second stage of drawing face. See last comment for
		# details.

		# Turn antialiasing on
		ctx.set_antialias(cairo.ANTIALIAS_DEFAULT)

		# Move Cairo's cursor to the start point
		ctx.move_to(*absolute_start_pos)

		# This is only for return to highlight edges when the face is
		# selected
		edges = []

		for edge_index, a_to_b in face.edges:
			# This is only for return to highlight edges when the face is
			# selected
			edges.append(edge_index)

			# Get the edge
			edge = self.all_edges[edge_index]

			# Set index of the current end point
			if a_to_b:
				end_index = edge.b
			else:
				end_index = edge.a

			# Get the current end point
			end = self.all_points[end_index]

			# Get screen position of the end point
			absolute_pos = self.absolute(end.pos, self.center)
			absolute_pos = self.screen_pos(absolute_pos, shift, zoom)
			
			# If edge is line, draw line
			if edge.type == "line":
				ctx.line_to(*absolute_pos)
			
			# If edge is Bezier curve, draw Bezier curve
			elif edge.type == "bezier":
				# If a is a start point
				if a_to_b:
					# Get screen position of the first Bezier control point
					# relative to the first point
					x = edge.c.pos[0] * zoom
					y = edge.c.pos[1] * zoom

					# Calculate screen position of the first Bezier control
					# point
					bcp1 = self.absolute((x, y), last_pos)

					# Get screen position of the second Bezier control point
					# relative to the second point
					x = edge.d.pos[0] * zoom
					y = edge.d.pos[1] * zoom

					# Calculate screen position of the second Bezier control
					# point
					bcp2 = self.absolute((x, y), absolute_pos)
				
				# If b is a start point
				else:
					# Get screen position of the first Bezier control point
					# relative to the first point
					x = edge.d.pos[0] * zoom
					y = edge.d.pos[1] * zoom

					# Calculate screen position of the first Bezier control
					# point
					bcp1 = self.absolute((x, y), last_pos)

					# Get screen position of the second Bezier control point
					# relative to the second point
					x = edge.c.pos[0] * zoom
					y = edge.c.pos[1] * zoom

					# Calculate screen position of the second Bezier control
					# point
					bcp2 = self.absolute((x, y), absolute_pos)

				# Get screen x and y coordinates of the end point
				x = absolute_pos[0]
				y = absolute_pos[1]

				# Define edge of the face
				ctx.curve_to(bcp1[0], bcp1[1], bcp2[0], bcp2[1], x, y)

			# Set screen position of the end point as last position
			last_pos = absolute_pos

		# Set line width
		ctx.set_line_width(1)

		# Draw the border
		ctx.stroke()

		# Decides whether or not edges should be highlighted
		if active and index in self.active_faces and (mode == "edit" or
				mode == "changing_hue" or
				mode == "changing_saturation" or
				mode == "changing_value" or
				mode == "changing_alpha" or
				mode == "moving_points" or
				mode == "rotating_points" or
				mode == "moving_bezier_control_point" or
				mode == "scaling_points" or
				mode == "selecting_box"):
			
			return edges
		else:
			return []

	# This function draws an edge.
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	# - "edge" is he edge, which is going to be drawn
	# - "index" is index of the edge (in "all_edges" list), it is used only for
	#   drawing ID of the edge
	# - "mode" is current mode ("object", "edit", etc.)
	# - "active" is a boolean parameter which says if the object is active
	# - "face_active" is True when a face with this edge is selected
	# - "shift" is screen position of top left canvas corner
	# - "zoom" is current canvas zoom
	# - "draw_non_real" is a boolean parameter, which says if helper lines
	#   should be drawn (for example when exporting, they should not be drawn)
	# - "face_edge" is boolean parameter, which says if this edge is an edge of
	#	some face
	def draw_edge(self, ctx, edge, index, mode, active, face_active, shift,
		zoom, draw_non_real, face_edge):
		# Get end points of the edge
		point_a = self.all_points[edge.a]
		point_b = self.all_points[edge.b]

		# Get position of line end points relative to the object center 
		a = point_a.pos
		b = point_b.pos

		# Calculate absolute position of the line end points
		a = self.absolute(a, self.center)
		b = self.absolute(b, self.center)

		# Calculate screen position
		a = self.screen_pos(a, shift, zoom)
		b = self.screen_pos(b, shift, zoom)

		# Check if the edge is active
		edge_active = index in self.active_edges
		
		# If edge is Bezier curve, draw Bezier curve
		if edge.type == "bezier":
			# if some edge has active Bezier control point
			if self.active_bezier_control_point != None:
				# if this edge has active Bezier control point
				if index == self.active_bezier_control_point[0]:
					# If first (c) Bezier control point is selected
					if self.active_bezier_control_point[1]:
						active_bcp = 1
					
					# If second (d) Bezier control point is selected
					else:
						active_bcp = 2
				
				# If no Bezier control point shuold be highlighted
				else:
					active_bcp = 0
			
			# If no Bezier control point shuold be highlighted
			else:
				active_bcp = 0

			# Draw Bezier curve
			edge.draw(ctx, a, b, edge_active, mode, active, active_bcp,
				face_active, shift, zoom, draw_non_real, face_edge)

		# If edge is line, draw line
		elif edge.type == "line":
			edge.draw(ctx, a, b, edge_active, mode, active, face_active, zoom,
				draw_non_real, face_edge)

	# This function draw the object center. It is represented by a cross.
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	# - "shift" is screen position of top left canvas corner
	# - "zoom" is current canvas zoom
	def draw_center(self, ctx, shift, zoom):
		# Set line color and width
		ctx.set_source_rgba(*self.center_color)
		ctx.set_line_width(self.center_width)

		# Get screen position of the center
		x = shift[0] + self.center[0] * zoom
		y = shift[1] + self.center[1] * zoom

		# Draw horizontal line of the cross
		ctx.move_to(x - self.center_radius, y)
		ctx.line_to(x + self.center_radius, y)
		ctx.stroke()

		# Draw (vertical line of the cross)
		ctx.move_to(x, y - self.center_radius)
		ctx.line_to(x, y + self.center_radius)
		ctx.stroke()

	# This function checks if mouse is currently over some point. If there is
	# some point, its index (in "all_points" list) is returned. Otherwise
	# "None" is returned. This function is usually called with mouse click.
	# - "mouse_pos" is canvas position of mouse cursor
	# - "zoom" is current canvas zoom
	def mouse_over_point(self, mouse_pos, zoom):
		# Since all points are hypotetically going to be check, for-cycle is
		# needed 
		for i, point in enumerate(self.all_points):
			# Get position of the point relative to the object center
			relative_pos = point.pos

			# Calculate absolute position of the point, so it can be compared
			# with (absolute) mouse position
			absolute_pos = self.absolute(relative_pos, self.center)

			# if mouse is over point circle (with "point.radius" radius) return
			# index of the point
			if self.distance(mouse_pos, absolute_pos) <= point.radius / zoom:
				return i

		# If mouse is not over any point return "None"
		return None

	# This function checks if mouse is currently over some edge. Mouse does not
	# need to be exactly over the edge, since there is a threshold of
	# tolerance.If there is some edge, its index (in "all_edges" list) is
	# returned. Otherwise "None" is returned. This function is usually called
	# with mouse click.
	# - "mouse_pos" is canvas position of mouse cursor
	# - "zoom" is current canvas zoom
	def mouse_over_edge(self, mouse_pos, zoom):
		for index, edge in enumerate(self.all_edges):
			# Get indices of the end points
			index_a = edge.a
			index_b = edge.b

			# Get end points of the edge
			point_a = self.all_points[index_a]
			point_b = self.all_points[index_b]

			# Get relative position (to the center) of the end points
			a = point_a.pos
			b = point_b.pos

			# Calculate absolute position of the end points
			a = self.absolute(a, self.center)
			b = self.absolute(b, self.center)

			if edge.mouse_over(a, b, mouse_pos, zoom):
				return index

		# If mouse is not over any edge, return "None"
		else:		
			return None

	# Checks if mouse is currently over some face. If so, index of the face is
	# returned. Otherwise None is returned. This function is using "ray
	# casting algorithm".
	# - "mouse_pos" is canvas position of mouse cursor
	def mouse_over_face(self, mouse_pos):
		# For all faces
		for index, face in enumerate(self.all_faces):
			# Random ray direction vector
			ray_x = random.random() - 0.5
			ray_y = 0
			while ray_y == 0:
				ray_y = random.random() - 0.5

			# Get mouse coordinates
			mouse_x = mouse_pos[0]
			mouse_y = mouse_pos[1]

			# Number of edges, which have been crossed by the ray
			nu_crossed_edges = 0

			# Check if there is a intersection point between ray and some edge
			for edge_index, direction in face.edges:
				# Get the edge
				edge = self.all_edges[edge_index]

				# If the edge is line
				if edge.type == "line":

					# Get relative position
					a = self.all_points[edge.a].pos
					b = self.all_points[edge.b].pos

					# Get absolute canvas position
					a = self.absolute(a, self.center)
					b = self.absolute(b, self.center)

					# Get elements of edge direction vector (from a to b)
					edge_x = b[0] - a[0]
					edge_y = b[1] - a[1]

					# Solve system of two linear equations:
					# 1) mouse_x + p * ray_x = a[0] + q * edge_x,
					# 2) mouse_y + p * ray_y = a[1] + q * edge_y,
					# where p is positive and q is in (0, 1)

					c = ray_y / ray_x
					d = - edge_y + c * edge_x
					e = a[1] - mouse_y - c * (a[0] - mouse_x)
					q = e / d

					if q <= 0 or q >= 1:
						continue

					f = a[0] - mouse_x + edge_x * q
					p = f / ray_x

					# If the system of two linear equations has a solution, it
					# means that this edge is crossed by the ray. Therefore
					# number of crossed edges should be increased
					if p > 0:
						nu_crossed_edges += 1

				# If the edge is Bezier curve
				elif edge.type == "bezier":
					# Algorithm for checking if Bezier curve is crossde by the
					# ray has two parts. In the first part the Bezier curve is
					# going to be approximated by couple straight lines and in
					# the second part same algorithm, which was used for
					# checking if line is crossed, is going to be used on every
					# line, which approximates the Bezier curve.

					# Set size of step, where should be next point for line
					# approximation. For example dt = 0.05 means, that the
					# Bezier curve is going to be approximated by 20 lines.
					dt = 0.05

					# Get relative position of start and end point
					a_p = self.all_points[edge.a].pos
					b_p = self.all_points[edge.b].pos

					# Get absolute canvas position of start and end point
					a_p = self.absolute(a_p, self.center)
					b_p = self.absolute(b_p, self.center)

					# Get absolute canvas position of Bezier control points
					c_p = self.absolute(edge.c.pos, a_p)
					d_p = self.absolute(edge.d.pos, b_p)

					# Set current last point as the first point
					a = a_p

					# Set current step to 0
					t = 0

					# While 
					while t < 1:
						# Update step (where should the approximating point be)
						t += dt

						# Get canvas position of approximatin point
						x = edge.get_x_value_in_time(t, a_p, b_p, c_p, d_p)
						y = edge.get_y_value_in_time(t, a_p, b_p, c_p, d_p)
						
						# Create new temporary end point
						b = (x, y)

						# Get elements of edge direction vector (from a to b)
						edge_x = b[0] - a[0]
						edge_y = b[1] - a[1]

						# Solve system of two linear equations:
						# 1) mouse_x + p * ray_x = a[0] + q * edge_x,
						# 2) mouse_y + p * ray_y = a[1] + q * edge_y,
						# where p is positive and q is in (0, 1)

						c = ray_y / ray_x
						d = - edge_y + c * edge_x
						e = a[1] - mouse_y - c * (a[0] - mouse_x)
						q = e / d

						if q <= 0 or q >= 1:
							a = b
							continue

						f = a[0] - mouse_x + edge_x * q
						p = f / ray_x

						# If the system of two linear equations has a solution, it
						# means that this edge is crossed by the ray. Therefore
						# number of crossed edges should be increased
						if p > 0:
							nu_crossed_edges += 1

						# Set temporary end point as last point
						a = b

			# As the ray casting algorithm says, if number of crossed edges is
			# odd, start of the ray (mouse cursor) is in the polygon. Therefore
			# index of the face is returned
			if nu_crossed_edges % 2 == 1:
				return index

		# If mouse is over no face, None is returned
		else:
			return None

	# Functions, which handle specific actions ================================

	# Checks if edge is edge of some face. If so, True is returned, otherwise
	# False is returned
	# - "edge_index" is index of the edge
	def is_edge_of_face(self, edge_index):
		for face in self.all_faces:
			for i, direction in face.edges:
				if i == edge_index:
					return True
		return False

	# Chanches all active edges from lines to Bezier curves and vice versa
	def switch_edge(self):
		# For all active edges
		for index in self.active_edges:
			# Get edge
			edge = self.all_edges[index]

			# If the edge is a Bezier curve
			if edge.type == "bezier":
				# Create a new line
				new = Line(edge.a, edge.b)

				# Replace the Bezier curve by the newly created line
				self.all_edges[index] = new

			# If the edge is a line
			elif edge.type == "line":
				# Get ending points 
				a = self.all_points[edge.a]
				b = self.all_points[edge.b]

				# Create a new Bezier curve
				new = BezierCurve(edge.a, edge.b, a.pos, b.pos)

				# Replace the line by the newly created Bezier curve
				self.all_edges[index] = new

	# Flips all active points horizontally. The axis is a vertical line, which
	# goes through the object center
	def flip_points_horizontally(self):
		# For all active points
		for point_index in self.active_points:
			# Get point
			point = self.all_points[point_index]

			# Set new position of the point relative to the object center
			x = - point.pos[0]
			y = point.pos[1]
			point.pos = (x, y)

			# If the point, which was flipped is an ending point of some Bezier
			# curve, then Bezier control point, which belongs to the point
			# needs to change ist position as well in order not to deform the
			# Bezier curve.
			for edge in self.all_edges:
				if edge.type == "bezier":
					if point_index == edge.a:
						x = - edge.c.pos[0]
						y = edge.c.pos[1]
						edge.c.pos = (x, y)
					elif point_index == edge.b:
						x = - edge.d.pos[0]
						y = edge.d.pos[1]
						edge.d.pos = (x, y)

	# Flips all active points vertically. The axis is a horizontal line, which
	# goes through the object center
	def flip_points_vertically(self):
		# For all active points
		for point_index in self.active_points:
			# Get point
			point = self.all_points[point_index]
			

			# Set new position of the point relative to the object center
			x = point.pos[0]
			y = - point.pos[1]
			point.pos = (x, y)

			# If the point, which was flipped is an ending point of some Bezier
			# curve, then Bezier control point, which belongs to the point
			# needs to change ist position as well in order not to deform the
			# Bezier curve.
			for edge in self.all_edges:
				if edge.type == "bezier":
					if point_index == edge.a:
						x = edge.c.pos[0]
						y = - edge.c.pos[1]
						edge.c.pos = (x, y)
					elif point_index == edge.b:
						x = edge.d.pos[0]
						y = - edge.d.pos[1]
						edge.d.pos = (x, y)

	# Changes color of all active edges/faces. It is called from
	# "color_dialog" mode.
	# - "rgba" is the new color
	def change_color(self, rgba):
		# Set color of all active edges. Will do nothing if faces are active.
		for edge_index in self.active_edges:
			edge = self.all_edges[edge_index]
			edge.color_real = rgba

		# Set color of all active faces. Will do nothing if edges are active.
		for face_index in self.active_faces:
			face = self.all_faces[face_index]
			face.color_inactive = rgba

	# Moves all active points towards or away from the object center depending
	# on the mouse movement
	# - "last_pos" is screen position of the mouse on the last frame
	# - "new_pos" is screen position of the mouse on the current frame
	def scaling_points(self, last_pos, new_pos):
		# Calculate distances from mouse to the object center
		old_distance = self.distance(self.center, last_pos)
		new_distance = self.distance(self.center, new_pos)

		# This prevents dividing by zero 
		if old_distance != 0:
			# Calculate the scaling coefficient
			ratio = new_distance / old_distance

			# For all active points
			for point_index in self.active_points:
				# Get point
				point = self.all_points[point_index]
				
				# Get point position relative to the center
				x = point.pos[0]
				y = point.pos[1]

				# Set new point position relative to the center
				point.pos = (x * ratio, y * ratio)

				# If the point, which was flipped is an ending point of some
				# Bezier curve, then Bezier control point, which belongs to the
				# point needs to change ist position as well in order not to
				# deform the Bezier curve.
				for edge in self.all_edges:
					if edge.type == "bezier":
						if edge.a == point_index:
							x = edge.c.pos[0]
							y = edge.c.pos[1]
							edge.c.pos = (x * ratio, y * ratio)
						elif edge.b == point_index:
							x = edge.d.pos[0]
							y = edge.d.pos[1]
							edge.d.pos = (x * ratio, y * ratio)

	# Changes hue of color of all active edges/faces. Other color properties
	# are left unchanged.
	# - "hue" is the new value of hue
	def change_hue(self, hue):
		# Set hue of all active faces. Will do nothing if edges are active.
		for index in self.active_faces:
			face = self.all_faces[index]
			rgba = face.color_inactive
			hsv = colorsys.rgb_to_hsv(*rgba[:-1])
			rgb = colorsys.hsv_to_rgb(hue, hsv[1], hsv[2])
			face.color_inactive = (rgb[0], rgb[1], rgb[2], rgba[3])

		# Set hue of all active edges. Will do nothing if faces are active.
		for index in self.active_edges:
			edge = self.all_edges[index]
			rgba = edge.color_real
			hsv = colorsys.rgb_to_hsv(*rgba[:-1])
			rgb = colorsys.hsv_to_rgb(hue, hsv[1], hsv[2])
			edge.color_real = (rgb[0], rgb[1], rgb[2], rgba[3])

	# Changes saturation of color of all active edges/faces. Other color
	# properties are left unchanged.
	# - "saturation" is the new value of hue
	def change_saturation(self, saturation):
		# Set saturation of all active faces. Will do nothing if edges are
		# active.
		for index in self.active_faces:
			face = self.all_faces[index]
			rgba = face.color_inactive
			hsv = colorsys.rgb_to_hsv(*rgba[:-1])
			rgb = colorsys.hsv_to_rgb(hsv[0], saturation, hsv[2])
			face.color_inactive = (rgb[0], rgb[1], rgb[2], rgba[3])

		# Set saturation of all active edges. Will do nothing if faces are
		# active.
		for index in self.active_edges:
			edge = self.all_edges[index]
			rgba = edge.color_real
			hsv = colorsys.rgb_to_hsv(*rgba[:-1])
			rgb = colorsys.hsv_to_rgb(hsv[0], saturation, hsv[2])
			edge.color_real = (rgb[0], rgb[1], rgb[2], rgba[3])

	# Changes value of color of all active edges/faces. Other color properties
	# are left unchanged.
	# - "value" is the new value of hue
	def change_value(self, value):
		# Set value of all active faces. Will do nothing if edges are active.
		for index in self.active_faces:
			face = self.all_faces[index]
			rgba = face.color_inactive
			hsv = colorsys.rgb_to_hsv(*rgba[:-1])
			rgb = colorsys.hsv_to_rgb(hsv[0], hsv[1], value)
			face.color_inactive = (rgb[0], rgb[1], rgb[2], rgba[3])

		# Set value of all active edges. Will do nothing if faces are active.
		for index in self.active_edges:
			edge = self.all_edges[index]
			rgba = edge.color_real
			hsv = colorsys.rgb_to_hsv(*rgba[:-1])
			rgb = colorsys.hsv_to_rgb(hsv[0], hsv[1], value)
			edge.color_real = (rgb[0], rgb[1], rgb[2], rgba[3])

	# Changes alpha of color of all active edges/faces. Other color properties
	# are left unchanged.
	# - "alpha" is the new value of hue
	def change_alpha(self, alpha):
		# Set alpha of all active faces. Will do nothing if edges are active.
		for index in self.active_faces:
			face = self.all_faces[index]
			rgba = face.color_inactive
			face.color_inactive = (rgba[0], rgba[1], rgba[2], alpha)

		# Set alpha of all active edges. Will do nothing if faces are active.
		for index in self.active_edges:
			edge = self.all_edges[index]
			rgba = edge.color_real
			edge.color_real = (rgba[0], rgba[1], rgba[2], alpha)

	# Moves one of Bezier control points accroding to mouse movement
	# - "dx" is horizontal change of mouse position between current and last
	#	frame
	# - "dy" is vertical change of mouse position between current and last
	#	frame
	def move_bezier_control_point(self, dx, dy):
		edge_index, c_or_d = self.active_bezier_control_point
		edge = self.all_edges[edge_index]
		edge.move_bezier_control_point(dx, dy, c_or_d)

	# Select all points which are inside the selection box
	# - "a" is screen position of the first corner of the selection box
	# - "b" is screen position of the second corner of the celection box
	def selection_box(self, a, b):
		if a[0] <= b[0]:
			left = a[0]
			right = b[0]
		else:
			left = b[0]
			right = a[0]

		if a[1] <= b[1]:
			top = a[1]
			bottom = b[1]
		else:
			top = b[1]
			bottom = a[1]

		# For all points
		for index, point in enumerate(self.all_points):
			# Get canvas position of the point
			canvas_x = point.pos[0] + self.center[0]
			canvas_y = point.pos[1] + self.center[1]

			# Check from all sides, if point is within the selection box
			left2 = left <= canvas_x
			right2 = canvas_x <= right
			top2 = top <= canvas_y
			bottom2 = canvas_y <= bottom

			# If the point is inside the selection box and had not been
			# selected, select it
			if left2 and right2 and top2 and bottom2:
				if not index in self.active_points:
					self.active_points.append(index)

	# Creates Bezier curve between two points
	# - "a_index" is index of the first point
	# - "b_index" is index of the second point
	def create_bezier(self, a_index, b_index):
		# Get points
		a = self.all_points[a_index]
		b = self.all_points[b_index]

		# Get canvas position of the points
		a_pos = self.absolute(a.pos, self.center)
		b_pos = self.absolute(b.pos, self.center)

		# Create Bezier curve
		bezier = BezierCurve(a_index, b_index, a_pos, b_pos)

		# Add the Bezier curve to "all_edges" list
		self.all_edges.append(bezier)

		# Make the Bezier curve as the only active edge
		index = len(self.all_edges) - 1
		self.active_edges = [index]
		self.active_points = []

	# Selects all edges. If all edges already had been selected, deselect them
	# all
	def select_all_edges(self):
		nu_edges = len(self.all_edges)
		if len(self.active_edges) == nu_edges:
			self.active_edges = []
		else:
			self.active_edges = list(range(nu_edges))
			self.active_points = []
			self.active_faces = []

			# Deactivate Bezier control points
			self.active_bezier_control_point = None

	# Selects all faces. If all faces already had been selected, deselect them
	# all
	def select_all_faces(self):
		nu_faces = len(self.all_faces)
		if len(self.active_faces) == nu_faces:
			self.active_faces = []
		else:
			self.active_faces = list(range(nu_faces))
			self.active_points = []
			self.active_edges = []
			
			# Deactivate Bezier control points
			self.active_bezier_control_point = None

	# Selects point, edge or face of the active object
	# - "mouse_pos" is canvas position of the mouse
	# - "ctrhl_held" is a boolean parameter, which says if Ctrl key is held
	# - "zoom" is current canvas zoom
	def select_part(self, mouse_pos, ctrl_held, zoom):
		# Selection of points has higher priority compared to selecting
		# edges/faces. There is an exception if some edges have been already
		# selected and left Ctrl is held. If this  occurs "selecting_edges"
		# returns True.
		if not (self.selecting_edges(ctrl_held) or
			self.selecting_faces(ctrl_held)):
			# Get index "point_index" of point, which is under mouse cursor
			point_index = self.mouse_over_point(mouse_pos, zoom)

			# If mouse is over some point (with index "point_index")
			if point_index != None:
				# Remove all edges from object's "active_edges", because
				# now are points being selected
				self.active_edges = []
				self.active_faces = []

				# Add (or remove) the point index to "active_points" list
				# if left Ctrl is held. Other indices leave without change.
				if ctrl_held:
					# Remove "point_index" from "active_points" if it
					# already is in the list
					if point_index in self.active_points:
						self.active_points.remove(point_index)

					# Add "point_index" to "active_points" if it is not
					# in "active_points" list yet
					else:
						self.active_points.append(point_index)

				# If left Ctrl is not held remove all point indices, which
				# are already in the "active_points" list, and replace them
				# by index of the new point
				else:
					self.active_points = [point_index]

				# Deactivate Bezier control points
				self.active_bezier_control_point = None

				# Point has been selected, so no other part can be selected
				# anymore
				return True

			# Empty "active_points" list if mouse is not over any point
			# (of active object)
			elif not ctrl_held:
				self.active_points = []

		# Selecting Bezier control has higher priority than selecting edges or
		# face. There is an exception if some edges have been already
		# selected and left Ctrl is held. If this  occurs "selecting_edges"
		# returns True. Also if points is not beneath the mouse, but a Bezier
		# control point is, the control point is not going to be selected if
		# some regular points have been selected and Ctrl is being held.
		if not (self.selecting_points(ctrl_held) or
			self.selecting_edges(ctrl_held) or
			self.selecting_faces(ctrl_held)):

			# For all Bezier curves
			for edge_index, edge in enumerate(self.all_edges):
				if edge.type == "bezier":
					# Get indices of its ending points
					a_index = edge.a
					b_index = edge.b

					# Get ending points
					a = self.all_points[a_index]
					b = self.all_points[b_index]

					# Get canvas position of the ending points
					a_pos = self.absolute(a.pos, self.center)
					b_pos = self.absolute(b.pos, self.center)

					# If mouse is over the first Bezier control point
					if edge.c.mouse_over(a_pos, mouse_pos, zoom):
						# Set the first Bezier control point of the edge as
						# active. True means it is the first (c) point, not
						# the second (d).
						self.active_bezier_control_point = (edge_index, True)
						self.active_points = []
						self.active_edges = []
						self.active_faces = []

						# Bezier control point has been selected, so no other
						# part can be selected anymore
						return True

					# If mouse is over the first Bezier control point
					elif edge.d.mouse_over(b_pos, mouse_pos, zoom):
						# Set the second Bezier control point of the edge as
						# active. True means it is the second (d) point, not
						# the first (c).
						self.active_bezier_control_point = (edge_index, False)
						self.active_points = []
						self.active_edges = []
						self.active_faces = []

						# Bezier control point has been selected, so no other
						# part can be selected anymore
						return True

					# If mouse is over no control point (of the edge)
					else:
						self.active_bezier_control_point = None

		# If mouse is not over any point and points are not being selected,
		# which means, that either "active_points" is empty or it is not
		# empty but left Ctrl is held
		if not (self.selecting_points(ctrl_held) or
			self.selecting_faces(ctrl_held)):
			# Get index "edge_index" of edge, which is under mouse cursor
			edge_index = self.mouse_over_edge(mouse_pos, zoom)

			# If mouse is over some edge (with index "edge_index")
			if edge_index != None:
				self.active_faces = []

				# Add (or remove) the edge index to "active_edges" list if
				# left Ctrl is held. Other indices leave without change.
				if ctrl_held:
					# Remove "edge_index" from "active_edges" if it already
					# is in the list
					if edge_index in self.active_edges:
						self.active_edges.remove(edge_index)
					
					# Add "edge_index" to "active_edges" if it is not in
					# "active_edges" list yet
					else:
						self.active_edges.append(edge_index)
				else:
					self.active_edges = [edge_index]

				# Deactivate Bezier control points
				self.active_bezier_control_point = None

				# Edge has been selected, so no face can be selected
				return True
			
			# Empty "active_edges" list if mouse is not over any edge (of
			# active object)
			elif not ctrl_held:
				self.active_edges = []

		# If there are some points or edges already selected and Ctrl is being
		# held, it will not be possible to select face
		if (not (self.selecting_points(ctrl_held) or 
			self.selecting_edges(ctrl_held))):
			# Get index of face, which is beneath the mouse. None if there is
			# no point beneath.
			face_index = self.mouse_over_face(mouse_pos)
			
			# If mouse is over some face
			if face_index != None:
				# If Ctrl is held select face, which had not been selected and
				# vice versa
				if ctrl_held:
					if face_index in self.active_faces:
						self.active_faces.remove(face_index)
					else:
						self.active_faces.append(face_index)
				
				# If Ctrl is not held make the face the only active face
				else:
					self.active_faces = [face_index]

				# Deactivate Bezier control points
				self.active_bezier_control_point = None

				# Face has been selected
				return True
			
			# If mouse is over no face and Ctrl is not held, deactivate all
			# faces
			elif not ctrl_held:
				self.active_faces = []

		# Return False if nothing has been selected
		return False

	# Creates face, whose border will consist of all active edges. If currently
	# active edges cannot form a loop or there is some addition edge, face will
	# not be created
	def create_face(self):
		# At least one edge needs to be active
		if len(self.active_edges) >= 1:
			# Get the first active edge
			edge_index = self.active_edges[0]
			edge = self.all_edges[edge_index]

			# List "used" has the same lenght as list "active_edges". It
			# contains booleans. True means, that edge, whose index is on the
			# same place in "active_edges" as the True value is in "used", has
			# been already used in the loop, which is going to be created.
			used = [False for edge in self.active_edges]

			# Loop is a list of tuples. First value in the tuple is the index
			# of edge, which is used in the loop. Second value in the tuple
			# means if when going trough the loop we will go from a to b (of
			# this edge) while going through this edge. True means a to b,
			# while False means b to a.
			loop = [(edge_index, True)]

			# Get start index
			start = edge.a

			# Get temporary end index
			end = edge.b

			# Set the first edge as used
			used[0] = True

			# Check if the loop is finished. It is finsihed if there are no
			# more edges to build the loop or if we get to some point in the
			# loop (close the loop, not necessarily at the beggining).
			while not self.is_loop_finished(end, loop, used):
				# For all active edges
				for i, j in enumerate(self.active_edges):
					# If the edge has not been used yet
					if not used[i]:
						# Get the edge
						edge = self.all_edges[j]

						# Get indices of its ending points
						a = edge.a
						b = edge.b

						# If the edge can be added to the loop
						if end == a:
							# Set the edge as used
							used[i] = True

							# Update temporary ending point
							end = b

							# Add the edge to the loop
							loop.append((j, True))

							# Look for next edge
							break

						# If the edge can be added to the loop
						elif end == b:
							# Set the edge as used
							used[i] = True

							# Update temporary ending point
							end = a

							# Add the edge to the loop
							loop.append((j, False))

							# Look for next edge
							break
				
				# If no edge can be added to the loop
				else:
					print("Face cannot be created")
					return False

			# Create face if the loop is closed and all edges have been used
			if start == end and self.are_all(used, True):
				# Create face
				face = Face(loop)

				# Add the new face to "all_faces"
				self.all_faces.append(face)

				# Make the new face as the only active object part
				index = len(self.all_faces) - 1
				self.active_faces = [index]
				self.active_edges = []

				print("Face has been created")
				return True

			# Clean loop could not be created
			else:
				print("Face cannot be created")
				return False
		
		# If no edge is active
		else:
			print("Face cannot be created")
			return False

	# This function adds a new point to the object (or more precisely to its
	# "all_points" list). It also makes the new point active, while
	# deactivating all other points.
	# - "pos" is the position of the new point in format of tuple (x, y)
	def add_point(self, pos):
		# Create a new point. Its position relative to the object center will
		# be calculated automatically.
		new_point = Point(pos, self.center)

		# Add the new point to "all_points" list
		self.all_points.append(new_point)

		# Make the new point the only active point of this object
		self.active_points = [len(self.all_points) - 1]

		self.active_edges = []
		self.active_faces = []

		# Deactivate Bezier control points
		self.active_bezier_control_point = None

	# This function adds a new point to the object (or more precisely to its
	# "all_points" list). It will also automatically create line between the
	# new point and all points, which had been active. It also makes the new
	# point active, while deactivating all other points.
	# - "new_pos" is the canvas position of the new point
	def add_point_with_edges(self, new_pos):
		# Create a new point. Its position relative to the object center will
		# be calculated automatically.
		new_point = Point(new_pos, self.center)

		# Add the new point to "all_points" list
		self.all_points.append(new_point)

		# Get the new index
		new_index = len(self.all_points) - 1

		# Create lines between the new point and points, which has been active
		for index in self.active_points:
			self.create_line(new_index, index)

		# Make the new point the only active part of this object
		self.active_points = [new_index]
		self.active_edges = []
		self.active_faces = []

		# Deactivate Bezier control points
		self.active_bezier_control_point = None
			
	# This function adds a new point to the object (or more precisely to its
	# "all_points" list). It will also automatically create Bezier curve
	# between the new point and all points, which had been active. It also
	# makes the new point active, while deactivating all other points.
	# - "new_pos" is the canvas position of the new point
	def add_point_with_beziers(self, new_pos):
		# Create a new point. Its position relative to the object center will
		# be calculated automatically.
		new_point = Point(new_pos, self.center)

		# Add the new point to "all_points" list
		self.all_points.append(new_point)

		# Get the new index
		new_index = len(self.all_points) - 1

		# Create Bezier curves between the new point and points, which has been
		# active
		for index in self.active_points:
			self.create_bezier(new_index, index)

		# Make the new point the only active part of this object
		self.active_points = [new_index]
		self.active_edges = []
		self.active_faces = []

		# Deactivate Bezier control points
		self.active_bezier_control_point = None

	# This function selects all points, if there is at least one inactive
	# point. If all points are active, this function will deactivate them all.
	def select_all_points(self):
		# How many points does this object have
		nu_points = len(self.all_points)

		# If "active_points" list has as many indicies as "all_points" list,
		# then are all points selected. Therefore all points shall be
		# deselected by setting "active_points list empty.
		if len(self.active_points) == nu_points:
			self.active_points = []

		# If at least one points had not been selected, then select all points
		# by filling "active_points" with indices of all points.
		else:
			self.active_points = list(range(nu_points))
			self.active_edges = []
			self.active_faces = []

			# Deactivate Bezier control points
			self.active_bezier_control_point = None

	# This function creates a new line between two selected points. This is the
	# reason why exactly two points need to be selected and why otherwise is
	# the line not created. This function also makes the newly added line the
	# only active edge. Also all points are by this function automatically
	# deactivated.
	def connect_points(self):
		# If exatly two points are active
		if len(self.active_points) == 2:
			# Get indices of two points, which are going to be end points of
			# the new line
			a_index = self.active_points[0]
			b_index = self.active_points[1]

			# Get positions
			a = self.all_points[a_index]
			a_pos = self.absolute(a.pos, self.center)
			b = self.all_points[b_index]
			b_pos = self.absolute(b.pos, self.center)

			# Create line
			self.create_line(a_index, b_index)

			# Deactivate all points, because it is not possible to have points
			# and edges active at the same time
			self.active_points = []

	# This function creates a new Bezier curve between two selected points.
	# This is the reason why exactly two points need to be selected and why
	# otherwise is the line not created. This function also makes the newly
	# added Bezier curve the only active edge. Also all points are by this
	# function automatically deactivated.
	def connect_points_with_bezier(self):
		if len(self.active_points) == 2:
			a_index = self.active_points[0]
			b_index = self.active_points[1]
			self.create_bezier(a_index, b_index)

	# Creates line between two points
	# - "index1" is index of the first point
	# - "index2" is index of the second point
	def create_line(self, index1, index2):
		# Create the new line
		line = Line(index1, index2)

		# Add the line to "all_edges" list
		self.all_edges.append(line)

		# Get new index
		index = len(self.all_edges) - 1

		# Make the new line the only active line
		self.active_edges = [index]

	# Deletes all active points/edges/faces. If a point is deleted, all edges,
	# which have this point as an eding points are deleted as well. Similarly,
	# if an edge is deleted, all faces, which have this edge in its border, are
	# delted too.
	def delete_part(self):
		if len(self.active_points) > 0:
			self.delete_points(self.active_points)

		elif len(self.active_edges) > 0:
			self.delete_edges(self.active_edges)

		elif len(self.active_faces) > 0:
			self.delete_faces(self.active_faces)

	# Deletes all faces, whose indicies are in the list
	# - "faces" is a list of all face which are going to be deleted
	def delete_faces(self, faces):
		# For all faces in "faces" list from the end of the "all_faces" list to
		# the beginning. This order is important, because otherwise indices
		# would change during the process.
		for face_index in sorted(faces, reverse=True):
			# Delete the face
			self.all_faces.pop(face_index)

		# Deactivate all faces
		self.active_faces = []

	# Deletes all points, whose indicies are in the list
	# - "points" is a list of all points which are going to be deleted
	def delete_points(self, points):
		# For all points in "points" list from the end of the "all_points" list
		# to the beginning. This order is important, because otherwise indices
		# would change during the process.
		for point_index in sorted(points, reverse=True):
			# Delete the point
			self.all_points.pop(point_index)

			# Check, if there are some edges, which must be deleted as well.
			# Initialy set list of such edges to an empty list
			edge_indices_to_delete = []

			# For all edges
			for edge_index, edge in enumerate(self.all_edges):
				# if the edge has the point as its ending point
				if edge.a == point_index:
					edge_indices_to_delete.append(edge_index)
					continue

				# If the edge has ending point, whose index is bigger than
				# index of the point, which is going to be deleted, it needs to
				# be decreased by one. Otherwise the index would refer to
				# different point, after the deleting procces.
				elif edge.a > point_index:
					edge.a -= 1

				# if the edge has the point as its ending point
				if edge.b == point_index:
					edge_indices_to_delete.append(edge_index)
					continue

				# If the edge has ending point, whose index is bigger than
				# index of the point, which is going to be deleted, it needs to
				# be decreased by one. Otherwise the index would refer to
				# different point, after the deleting procces.
				elif edge.b > point_index:
					edge.b -= 1

			# Delete all edges, which have point, which are going to be
			# deleted, as their ending points
			self.delete_edges(edge_indices_to_delete)

		# Deactivate all points
		self.active_points = []

	# Deletes all edges, whose indicies are in the list
	# - "edges" is a list of all edges+ which are going to be deleted
	def delete_edges(self, edges):
		# For all edges in "edges" list from the end of the "all_edges" list to
		# the beginning. This order is important, because otherwise indices
		# would change during the process.
		for edge_index in sorted(edges, reverse=True):
			# Delete the edge
			self.all_edges.pop(edge_index)

			# Check if there are some faces, which have the edge as part of
			# their border
			face_indices_to_delete = []

			# For all faces
			for face_index, face in enumerate(self.all_faces):
				# For all edges of the face
				for i, index_and_direction in enumerate(face.edges):
					# if the edge is part of face's border
					if edge_index == index_and_direction[0]:
						face_indices_to_delete.append(face_index)
						break

					# If index of some edge in the face's border is higher than
					# index of the point, it neads to be decreased by one.
					# Otherwise the index would refer to different point, after
					# the deleting procces.
					elif edge_index < index_and_direction[0]:
						new_index = face.edges[i][0] - 1
						direction = face.edges[i][1]
						face.edges[i] = (new_index, direction)

			# Delete faces, which have some of the deleted edges as part of its
			# border
			self.delete_faces(face_indices_to_delete)

		# Deactivate all edges
		self.active_edges = []

	# Little helper functions =================================================
	
	# Checks if point is an ending point of some edge
	# - "index" is index of the point
	def is_ending_point(self, index):
		for edge in self.all_edges:
			if (index == edge.a or index == edge.b):
				return True
		return False

	# Function "selecting_points" is a little helper function, which helps to
	# make code a bit more clean. It is used in "on_click" function. When mouse
	# is over some point or more points are going to be selected, because some
	# already have been and left Ctrl is held, True is returned.
	# - "ctrl_held" is a boolean parameter, which says if Ctrl key is held
	def selecting_points(self, ctrl_held):
		if len(self.active_points) > 0 and ctrl_held:
			return True
		else:
			return False

	# Function "selecting_edges" is a little helper function, which helps to
	# make code a bit more clean. It is used in "on_click" function. When more
	# edges are going to be selected, because some already have been and left
	# Ctrl is held, True is returned.
	# - "ctrl_held" is a boolean parameter, which says if Ctrl key is held
	def selecting_edges(self, ctrl_held):
		if len(self.active_edges) > 0 and ctrl_held:
			return True
		else:
			return False

	# Function "selecting_faces" is a little helper function, which helps to
	# make code a bit more clean. It is used in "on_click" function. When more
	# faces are going to be selected, because some already have been and left
	# Ctrl is held, True is returned.
	# - "ctrl_held" is a boolean parameter, which says if Ctrl key is held
	def selecting_faces(self, ctrl_held):
		if len(self.active_faces) > 0 and ctrl_held:
			return True
		else:
			return False

	# Function "are_all" is a little helper function, which returns True if all
	# items in the array has the demanded value
	# - "array" is an array, which is being checked
	# - "value" is the demanded value
	def are_all(self, array, value):
		for i in array:
			if i != value:
				return False
		return True

	# Check if the loop is finished. It is finsihed if there are no more edges
	# to build the loop or if we get to some point in the loop (close the loop,
	# not necessarily at the beggining).
	# - "end" is index of teporary end of the (not finished?) loop
	# - "loop" is a list of tuples. First value in the tuple is the index of
	#	edge, which is used in the loop. Second value in the tuple means if 
	#	when going trough the loop we will go from a to b (of this edge) while
	#	going through this edge. True means a to b, while False means b to a.
	# - "used" is a list, which has the same lenght as list "active_edges". It
	#	contains booleans. True means, that edge, whose index is on the same
	#	place in "active_edges" as the True value is in "used", has been
	#	already used in the loop, which is going to be created.
	def is_loop_finished(self, end, loop, used):
		if self.is_in_loop(end, loop[:-1]) or self.are_all(used, True):
			return True
		else:
			return False

	# Checks if point is in list (loop) of edges as an ending point
	# - "point" is index of the point
	# - "loop" is a list of tuples. First value in the tuple is the index of
	#	edge, which is used in the loop. Second value in the tuple means if 
	#	when going trough the loop we will go from a to b (of this edge) while
	#	going through this edge. True means a to b, while False means b to a.
	def is_in_loop(self, point, loop):
		for edge_index, direction in loop:
			edge = self.all_edges[edge_index]
			if point == edge.a or point == edge.b:
				return True
		return False

	# Calculates angle between direction vectors (x, y) and (1, 0) using
	# cyclometric (arcus tangens) function. Function "calculate_angle" needs to
	# check if "x" is not zero, because "y" is going to be divided by "x". This
	# function also checks if "x" and "y" are positive or negative, because
	# arcus tangens has range (- pi/2, pi/2), but angle is going to have range
	# [0, 2*pi]. Note that since "y" values are negative above the (0, 0) and
	# not below as it is usually shown in math, whole "circle" of angle values
	# is going to be vertically flipped compared to the standard math plot.
	# - "x" is horizontal position
	# - "y" is vertical position
	def calculate_angle(self, x, y):
		if x > 0:
			if y >= 0:
				return math.atan(y / x)
			else:
				return 2*math.pi + math.atan(y / x)
		elif x < 0:
			return math.pi - math.atan(y / -x)
		else:
			if y > 0:
				return math.pi / 2
			elif y < 0:
				return math.pi * 3 / 2
			else:
				return 0

	# This function returns point position relative to the origin. Inverse
	# function to this function is "absolute".
	# - "pos" is absolute position of the point in format of tuple (x, y)
	# - "origin" is (absolute) position of the origin in format of tuple (x, y)
	def relative(self, pos, origin):
		x = pos[0] - origin[0]
		y = pos[1] - origin[1]
		return (x, y)

	# This function calculates average position (or mass center) of points in
	# list "points". Center position is returned.
	# - "points" is a list of all points from which the center is going to be
	#   calculated
	def calculate_center(self, points):
		x = 0
		y = 0
		for dx, dy in points:
			x += dx
			y += dy
		x /= len(points)
		y /= len(points)
		return (x, y)

	# This function calculates absolute point position from the relative point
	# position to the origin. Inverse function to this function is "relative".
	# - "pos" is relative position of the point in format of tuple (x, y)
	# - "origin" is (absolute) position of the origin in format of tuple (x, y)
	def absolute(self, pos, origin):
		x = pos[0] + origin[0]
		y = pos[1] + origin[1]
		return (x, y)

	# Function "distance" is a little helper function, which calculates
	# distance between two point positions
	# - "a" is position of the first point
	# - "b" is position of the second point
	def distance(self, a, b):
		x = abs(a[0] - b[0])
		y = abs(a[1] - b[1])
		return math.sqrt(x**2 + y**2)

	# Calculates screen position from canvas position, canvas shift and zoom
	# - "canvas_pos" is canvas position, which is going to be converted
	# - "shift" is screen position of top left canvas corner
	# - "zoom" is current canvas zoom
	def screen_pos(self, canvas_pos, shift, zoom):
		x = shift[0] + canvas_pos[0] * zoom
		y = shift[1] + canvas_pos[1] * zoom
		return (x, y)


# =============================================================================
# This is object represents of the two control points of some (cubic) Bezier
# curve
# =============================================================================
class BezierPoint():
	# - "point_index" is index of point with pos a
	# - "b" is second eding point
	def __init__(self, point_index, a, b):
		dx = b[0] - a[0]
		dy = b[1] - a[1]

		# Relative to point with index "point_index"
		self.pos = (dx/4, dy/4)
		self.point = point_index

		# Graphics
		self.color_inactive = (0, 0, 0, 1)
		self.color_active = (1, 0, 0, 1)
		self.radius = 4

	# - "pos" is absolute position of the point whose Bezier point this is
	def draw(self, ctx, pos, active, shift, zoom):
		if active:
			ctx.set_source_rgba(*self.color_active)
		else:
			ctx.set_source_rgba(*self.color_inactive)

		# Point itself
		ctx.set_line_width(1)

		x = pos[0] + self.pos[0] * zoom
		y = pos[1] + self.pos[1] * zoom

		ctx.arc(x, y, self.radius, 0, 2*math.pi)
		ctx.stroke()

		# Connecting line
		ctx.set_dash([2])
		ctx.set_line_width(1)
		ctx.move_to(*pos)
		ctx.line_to(x, y)
		ctx.stroke()
		ctx.set_dash([])

	# "point_pos" is absolute position of the point whose control point this is
	def mouse_over(self, point_pos, mouse_pos, zoom):
		x = self.pos[0] + point_pos[0]
		y = self.pos[1] + point_pos[1]
		if self.distance((x, y), mouse_pos) <= self.radius / zoom:
			return True
		else:
			return False

	# Function "distance" is a little helper function, which calculates
	# distance between two point positions
	# - "a" is position of the first point
	# - "b" is position of the second point
	def distance(self, a, b):
		x = abs(a[0] - b[0])
		y = abs(a[1] - b[1])
		return math.sqrt(x**2 + y**2)

	# Changes position of the Bezier control point
	# - "dx" is horizontal change of mouse position between current and last
	#	frame
	# - "dy" is vertical change of mouse position between current and last
	#	frame
	def move(self, dx, dy):
		x = self.pos[0] + dx
		y = self.pos[1] + dy
		self.pos = (x, y)

	# Calculates screen position from canvas position, shift and zoom
	# - "canvas_pos" is canvas position, which is going to be converted
	# - "shift" is screen position of top left canvas corner
	# - "zoom" is current canvas zoom
	def screen_pos(self, canvas_pos, shift, zoom):
		x = shift[0] + canvas_pos[0] * zoom
		y = shift[1] + canvas_pos[1] * zoom
		return (x, y)


# =============================================================================
# This class represents a cubic Bezier curve. Bezier curve is a type of edge.
# =============================================================================
class BezierCurve():
	# This function defines end points of the line and graphics properties
	# - "a_index" is index of the first ending point
	# - "b_index" is index of the second ending point
	# - "a_pos" is postion of the first ending point relative to the object
	#	center
	# - "b_pos" is postion of the second ending point relative to the object
	#	center
	def __init__(self, a_index, b_index, a_pos, b_pos):
		# Set indices of ending points
		self.a = a_index
		self.b = b_index

		# Set Bezier control points
		self.c = BezierPoint(a_index, a_pos, b_pos)
		self.d = BezierPoint(b_index, b_pos, a_pos)

		# Set edge type
		self.type = "bezier"

		# Set width of the line itself
		self.width = 0

		# Set width of line showing that there is a line (visible even when
		# width is set to 0, highligheted when selected etc.)
		self.width2 = 1

		# Set color of active edge (while in edit-based mode)
		self.color_active = (0, 0, 1, 1)

		# Set color of inactive edge (or active edge in object-based mode)
		self.color_inactive = (0, 0, 0, 1)

		# Set real color (this is the color, which can be changed)
		self.color_real = (0.3, 0.3, 0.3, 1)

	# This function draw Bezier curve from a to b. Color of the line depends
	# and its "active" property and the mode.
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	# - "a" is screen position of the first ending point
	# - "b" is screen position of the second ending point
	# - "active" is a boolean parameter which says if the edge is active or not
	# - "mode" is current mode ("object", "edit" etc.)
	# - "active_object" is a boolean parameter which says if the object active
	# - "active_bcp" indicates which Bezier control point is active. 0 - none,
	#	1 - the first (c), 2 - the second (d)
	# - "face_active" is True when a face with this edge is selected
	# - "shift" is screen position of top left canvas corner
	# - "zoom" is current canvas zoom
	# - "draw_non_real" is a boolean parameter, which says if helper lines
	#   should be drawn (for example when exporting, they should not be drawn)
	# - "face_edge" is boolean parameter, which says if this edge is an edge of
	#	some face
	def draw(self, ctx, a, b, active, mode, active_object, active_bcp,
		face_active, shift, zoom, draw_non_real, face_edge):
		# Draw the real edge first

		# Set color
		color = self.color_real
		ctx.set_source_rgba(*color)

		# Set line width
		ctx.set_line_width(self.width * zoom)
		
		# Draw line from a to b
		ctx.move_to(*a)

		# Get screen position of the first Bezier control point
		rel_x = self.c.pos[0] * zoom
		rel_y = self.c.pos[1] * zoom
		c = self.absolute((rel_x, rel_y), a)

		# Get screen position of the second Bezier control point
		rel_x = self.d.pos[0] * zoom
		rel_y = self.d.pos[1] * zoom
		d = self.absolute((rel_x, rel_y), b)

		# Draw the Bezier curve
		ctx.curve_to(c[0], c[1], d[0], d[1], b[0], b[1])
		ctx.stroke()

		# Now draw the non-real edge if it should be displayed
		if ((active_object and (mode == "edit" or
				mode == "changing_hue" or
				mode == "changing_saturation" or
				mode == "changing_value" or
				mode == "changing_alpha" or
				mode == "moving_points" or
				mode == "rotating_points" or
				mode == "moving_bezier_control_point" or
				mode == "scaling_points" or
				mode == "selecting_box")) or ((self.width == 0 or
				self.color_real[3] < 0.1) and draw_non_real and
				not face_edge)):
				
				# Select color depending on mode, activity etc.
				color = self.select_color(active, mode, active_object,
					face_active)
				ctx.set_source_rgba(*color)

				# Set line width
				ctx.set_line_width(self.width2)

				# Draw line from a to b
				ctx.move_to(*a)

				# Get screen position of the first Bezier control point
				rel_x = self.c.pos[0] * zoom
				rel_y = self.c.pos[1] * zoom
				c = self.absolute((rel_x, rel_y), a)

				# Get screen position of the second Bezier control point
				rel_x = self.d.pos[0] * zoom
				rel_y = self.d.pos[1] * zoom
				d = self.absolute((rel_x, rel_y), b)

				# Draw the Bezier curve
				ctx.curve_to(c[0], c[1], d[0], d[1], b[0], b[1])
				ctx.stroke()

		# Draw Bezier control points if they should be displayed
		if (active_object and (mode == "edit" or
				mode == "changing_hue" or
				mode == "changing_saturation" or
				mode == "changing_value" or
				mode == "changing_alpha" or
				mode == "moving_points" or
				mode == "rotating_points" or
				mode == "moving_bezier_control_point" or
				mode == "scaling_points" or
				mode == "selecting_box")):
				
				# If the first Bezier control point is active
				if active_bcp == 1:
					self.c.draw(ctx, a, True, shift, zoom)

				# If the first Bezier control point is not active
				else:
					self.c.draw(ctx, a, False, shift, zoom)

				# If the second Bezier control point is active
				if active_bcp == 2:
					self.d.draw(ctx, b, True, shift, zoom)
				
				# If the second Bezier control point is not active
				else:
					self.d.draw(ctx, b, False, shift, zoom)

	# Calculates screen position from canvas position, canvas shift and zoom
	# - "canvas_pos" is canvas position, which is going to be converted
	# - "shift" is screen position of top left canvas corner
	# - "zoom" is current canvas zoom
	def screen_pos(self, canvas_pos, shift, zoom):
		x = shift[0] + canvas_pos[0] * zoom
		y = shift[1] + canvas_pos[1] * zoom
		return (x, y)

	# Gets horizontal position of Bezier curve in time. The begging (a) of the
	# Bezier curve has time 0, while the end (b) has time 1
	# - "t" is time
	# - "a" is canvas position of the start point
	# - "b" is canvas position of the end point
	# - "c" is canvas position of the first Bezier control point
	# - "d" is canvas position of the second Bezier control point
	def get_x_value_in_time(self, t, a, b, c, d):
		# Used equation:
		# x(t) = (1-t)^3*p1x + 3t*(1-t)^2*p2x + 3t^2*(1-t)*p3x + t^3*p4x
		part1 = (1-t)**3 * a[0]
		part2 = 3 * t * (1-t)**2 * c[0]
		part3 = 3 * t**2 * (1-t) * d[0]
		part4 = t**3 * b[0]
		total = part1 + part2 + part3 + part4
		return total

	# Gets horizontal position of Bezier curve in time. The begging (a) of the
	# Bezier curve has time 0, while the end (b) has time 1
	# - "t" is time
	# - "a" is canvas position of the start point
	# - "b" is canvas position of the end point
	# - "c" is canvas position of the first Bezier control point
	# - "d" is canvas position of the second Bezier control point
	def get_y_value_in_time(self, t, a, b, c, d):
		# Used equation:
		# y(t) = (1-t)^3*p1y + 3t*(1-t)^2*p2y + 3t^2*(1-t)*p3y + t^3*p4y
		part1 = (1-t)**3 * a[1]
		part2 = 3 * t * (1-t)**2 * c[1]
		part3 = 3 * t**2 * (1-t) * d[1]
		part4 = t**3 * b[1]
		total = part1 + part2 + part3 + part4
		return total

	# This function returns appropriate color depending on edge activity and
	# the mode
	# - "active" is a boolean parameter which says if the edge is active or not
	# - "mode" is current mode ("object", "edit" etc.)
	# - "active_object" is a boolean parameter which says if the object active
	# - "face_active" is True when a face with this edge is selected
	def select_color(self, active, mode, active_object, face_active):
		if (active_object and (mode == "edit" or
				mode == "changing_hue" or
				mode == "changing_saturation" or
				mode == "changing_value" or
				mode == "changing_alpha" or
				mode == "moving_points" or
				mode == "rotating_points" or
				mode == "moving_bezier_control_point" or
				mode == "scaling_points" or
				mode == "selecting_box")):
			if active:
				return self.color_active
			elif face_active:
				return (0, 1, 0, 1)
			else:
				return self.color_inactive
		else:
			return self.color_inactive

	# Moves one of the Bezier control points
	# - "dx" is horizontal change of mouse position between current and last
	#	frame
	# - "dy" is vertical change of mouse position between current and last
	#	frame
	# - "c_or_d" say which Bezier control point shuold be moved. True means c,
	#	while False means d
	def move_bezier_control_point(self, dx, dy, c_or_d):
		if c_or_d:
			self.c.move(dx, dy)
		else:
			self.d.move(dx, dy)

	# This function calculates absolute point position from the relative point
	# position to the origin. Inverse function to this function is "relative".
	# - "pos" is relative position of the point in format of tuple (x, y)
	# - "origin" is (absolute) position of the origin in format of tuple (x, y)
	def absolute(self, pos, origin):
		x = pos[0] + origin[0]
		y = pos[1] + origin[1]
		return (x, y)

	# Checks if mouse is over the Bezier curve
	# - "a" is canvas position of the first ending point
	# - "b" is canvas position of the second ending point
	# - "mouse_pos" is canvas position of mouse cursor
	# - "zoom" is current canvas zoom
	def mouse_over(self, a, b, mouse_pos, zoom):
		# Set threshold for allowing imprecise clicks. If the line is
		# horizontal it can be click a little bit above or beneath the
		# line, but the click will not be recognized if it is a little bit
		# more left or right. In case of vertical line it is possible to
		# click a little bit more left or right, but not above or beneath
		# the line. It works similarly for differently rotated lines.
		threshold = 5 / zoom

		# Absolute positions of control points
		c = self.absolute(self.c.pos, a)
		d = self.absolute(self.d.pos, b)

		# Prepare approximation
		dt = 0.05
		t = 0

		# First point
		e = a

		# For all lines approximating the bezier curve
		while t <= 1:
			t += dt
			x = self.get_x_value_in_time(t, a, b, c, d)
			y = self.get_y_value_in_time(t, a, b, c, d)
			
			# Second point
			f = (x, y)


			# Calculate position of b and mouse relative to a
			f_rel_to_e = self.relative(f, e)
			mouse_rel_to_e = self.relative(mouse_pos, e)

			# Calculate angle of the line (vector b) and mouse vector
			f_angle = self.calculate_angle(*f_rel_to_e)
			mouse_angle = self.calculate_angle(*mouse_rel_to_e)

			# Calculate distances from b and mouse position to a
			distance_f = self.distance(e, f)
			distance_mouse = self.distance(e, mouse_pos)

			# Rotate b position
			f_rotated = (distance_f, e[1])

			# Rotate mouse position by same angle as b has been rotated
			angle = mouse_angle - f_angle
			x = distance_mouse * math.cos(angle)
			y = distance_mouse * math.sin(angle)
			mouse_rotated = (x, y)

			# Check side by sid eif mouse click is in appropriate rectangle
			# (around the line) Threshold is now used only in vertical
			# direction, since the line has been rotated in such way, that it
			# is horizontal now.
			left = 0 <= mouse_rotated[0]
			right = mouse_rotated[0] <= f_rotated[0]
			top = -self.width <= mouse_rotated[1] + threshold
			bottom = mouse_rotated[1] - threshold < self.width

			e = f

			# If mouse is in the rectangle, return index of the edge beneath
			# the mouse cursor.
			if left and right and top and bottom:
				return True
		
		else:
			return False

	# Calculates angle between direction vectors (x, y) and (1, 0) using
	# cyclometric (arcus tangens) function. Function "calculate_angle" needs to
	# check if "x" is not zero, because "y" is going to be divided by "x". This
	# function also checks if "x" and "y" are positive or negative, because
	# arcus tangens has range (- pi/2, pi/2), but angle is going to have range
	# [0, 2*pi]. Note that since "y" values are negative above the (0, 0) and
	# not below as it is usually shown in math, whole "circle" of angle values
	# is going to be vertically flipped compared to the standard math plot.
	# - "x" is horizontal position
	# - "y" is vertical position
	def calculate_angle(self, x, y):
		if x > 0:
			if y >= 0:
				return math.atan(y / x)
			else:
				return 2*math.pi + math.atan(y / x)
		elif x < 0:
			return math.pi - math.atan(y / -x)
		else:
			if y > 0:
				return math.pi / 2
			elif y < 0:
				return math.pi * 3 / 2
			else:
				return 0

	# This function returns point position relative to the origin. Inverse
	# function to this function is "absolute".
	# - "pos" is absolute position of the point in format of tuple (x, y)
	# - "origin" is (absolute) position of the origin in format of tuple (x, y)
	def relative(self, pos, origin):
		x = pos[0] - origin[0]
		y = pos[1] - origin[1]
		return (x, y)

	# Function "distance" is a little helper function, which calculates
	# distance between two point positions
	# - "a" is position of the first point
	# - "b" is position of the second point
	def distance(self, a, b):
		x = abs(a[0] - b[0])
		y = abs(a[1] - b[1])
		return math.sqrt(x**2 + y**2)


# =============================================================================
# This class represents line. Line is a type of edge.
# =============================================================================
class Line():
	# This function defines end points of the line and graphics properties
	# - "a_index" is index of the first ending point
	# - "b_index" is index of the second ending point
	def __init__(self, a_index, b_index):
		# Set indices of ending points
		self.a = a_index
		self.b = b_index

		# Set edge type
		self.type = "line"

		# Set width of the line itself
		self.width = 0

		# Set width of line showing that there is a line (visible even when
		# width is set to 0, highligheted when selected etc.)
		self.width2 = 1

		# Set color of active edge (while in edit-based mode)
		self.color_active = (0, 0, 1, 1)

		# Set color of inactive edge (or active edge in object-based mode)
		self.color_inactive = (0, 0, 0, 1)

		# Set real color (this is the color, which can be changed)
		self.color_real = (0.3, 0.3, 0.3, 1)

	# This function draw line from a to b. Color of the line depends and its
	# "active" property and the mode.
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	# - "a" is screen position of the first ending point
	# - "b" is screen position of the second ending point
	# - "active" is a boolean parameter which says if the edge is active or not
	# - "mode" is current mode ("object", "edit" etc.)
	# - "active_object" is a boolean parameter which says if the object active
	# - "face_active" is True when a face with this edge is selected
	# - "zoom" is current canvas zoom
	# - "draw_non_real" is a boolean parameter, which says if helper lines
	#   should be drawn (for example when exporting, they should not be drawn)
	# - "face_edge" is boolean parameter, which says if this edge is an edge of
	#	some face
	def draw(self, ctx, a, b, active, mode, active_object, face_active, zoom,
		draw_non_real, face_edge):
		# Draw the real line first

		# Set color
		ctx.set_source_rgba(*self.color_real)

		# Set line width
		ctx.set_line_width(self.width * zoom)

		# Draw line from a to b
		ctx.move_to(*a)
		ctx.line_to(*b)
		ctx.stroke()
		
		# Now raw the non-real line, if it shuold be drawn
		if ((active_object and (mode == "edit" or
				mode == "changing_hue" or
				mode == "changing_saturation" or
				mode == "changing_value" or
				mode == "changing_alpha" or
				mode == "moving_points" or
				mode == "rotating_points" or
				mode == "moving_bezier_control_point" or
				mode == "scaling_points" or
				mode == "selecting_box")) or ((self.width == 0 or
				self.color_real[3] < 0.1) and draw_non_real and
				not face_edge) ):

			# Set color depending on the edge activity and the mode
			color = self.select_color(active, mode, active_object, face_active)
			ctx.set_source_rgba(*color)

			# Set line width
			ctx.set_line_width(self.width2)

			# Draw line from a to b
			ctx.move_to(*a)
			ctx.line_to(*b)
			ctx.stroke()


	# This function returns appropriate color depending on edge activity and
	# the mode
	# - "active" is a boolean parameter which says if the edge is active or not
	# - "mode" is current mode ("object", "edit" etc.)
	# - "active_object" is a boolean parameter which says if the object active
	# - "face_active" is True when a face with this edge is selected
	def select_color(self, active, mode, active_object, face_active):
		if (active_object and (mode == "edit" or
				mode == "changing_hue" or
				mode == "changing_saturation" or
				mode == "changing_value" or
				mode == "changing_alpha" or
				mode == "moving_points" or
				mode == "rotating_points" or
				mode == "moving_bezier_control_point" or
				mode == "scaling_points" or
				mode == "selecting_box")):
			if active:
				return self.color_active
			elif face_active:
				return (0, 1, 0, 1)
			else:
				return self.color_inactive
		else:
			return self.color_inactive

	# Checks if mouse is over the line
	# - "a" is canvas position of the first ending point
	# - "b" is canvas position of the second ending point
	# - "mouse_pos" is canvas position of mouse cursor
	# - "zoom" is current canvas zoom
	def mouse_over(self, a, b, mouse_pos, zoom):
		# Set threshold for allowing imprecise clicks. If the line is
		# horizontal it can be click a little bit above or beneath the
		# line, but the click will not be recognized if it is a little bit
		# more left or right. In case of vertical line it is possible to
		# click a little bit more left or right, but not above or beneath
		# the line. It works similarly for differently rotated lines.
		threshold = 5 / zoom

		# Calculate position of b and mouse relative to a
		b_rel_to_a = self.relative(b, a)
		mouse_rel_to_a = self.relative(mouse_pos, a)

		# Calculate angle of the line (vector b) and mouse vector
		b_angle = self.calculate_angle(*b_rel_to_a)
		mouse_angle = self.calculate_angle(*mouse_rel_to_a)

		# Calculate distances from b and mouse position to a
		distance_b = self.distance(a, b)
		distance_mouse = self.distance(a, mouse_pos)

		# Rotate b position
		b_rotated = (distance_b, a[1])

		# Rotate mouse position by same angle as b has been rotated
		angle = mouse_angle - b_angle
		x = distance_mouse * math.cos(angle)
		y = distance_mouse * math.sin(angle)
		mouse_rotated = (x, y)

		# Check side by sid eif mouse click is in appropriate rectangle
		# (around the line) Threshold is now used only in vertical
		# direction, since the line has been rotated in such way, that it
		# is horizontal now.
		left = 0 <= mouse_rotated[0]
		right = mouse_rotated[0] <= b_rotated[0]
		top = -self.width <= mouse_rotated[1] + threshold
		bottom = mouse_rotated[1] - threshold < self.width

		# If mouse is in the rectangle, return index of the edge beneath
		# the mouse cursor.
		if left and right and top and bottom:
			return True
		else:
			return False

	# Calculates angle between direction vectors (x, y) and (1, 0) using
	# cyclometric (arcus tangens) function. Function "calculate_angle" needs to
	# check if "x" is not zero, because "y" is going to be divided by "x". This
	# function also checks if "x" and "y" are positive or negative, because
	# arcus tangens has range (- pi/2, pi/2), but angle is going to have range
	# [0, 2*pi]. Note that since "y" values are negative above the (0, 0) and
	# not below as it is usually shown in math, whole "circle" of angle values
	# is going to be vertically flipped compared to the standard math plot.
	# - "x" is horizontal position
	# - "y" is vertical position
	def calculate_angle(self, x, y):
		if x > 0:
			if y >= 0:
				return math.atan(y / x)
			else:
				return 2*math.pi + math.atan(y / x)
		elif x < 0:
			return math.pi - math.atan(y / -x)
		else:
			if y > 0:
				return math.pi / 2
			elif y < 0:
				return math.pi * 3 / 2
			else:
				return 0

	# This function returns point position relative to the origin. Inverse
	# function to this function is "absolute".
	# - "pos" is absolute position of the point in format of tuple (x, y)
	# - "origin" is (absolute) position of the origin in format of tuple (x, y)
	def relative(self, pos, origin):
		x = pos[0] - origin[0]
		y = pos[1] - origin[1]
		return (x, y)

	# Function "distance" is a little helper function, which calculates
	# distance between two point positions
	# - "a" is position of the first point
	# - "b" is position of the second point
	def distance(self, a, b):
		x = abs(a[0] - b[0])
		y = abs(a[1] - b[1])
		return math.sqrt(x**2 + y**2)

# =============================================================================
# This class represents a face of an object.
# =============================================================================
class Face():
	def __init__(self, loop):
		self.edges = loop
		self.color_inactive = (0.7, 0.7, 0.7, 1)
		self.color_active = (0.5, 1, 1, 1)

# =============================================================================
# This class represents a point of an object
# =============================================================================
class Point():
	# This function defines initial point position and graphics properties
	# - "pos" is absolute position of the new point
	# - "origin" is (absolute) position of the object center
	def __init__(self, pos, origin):
		# Set position of the point. The position "pos" is absolute, so
		# position relative to the object center needs to be calculated.
		self.pos = self.relative(pos, origin)

		# Point radius is used for drawing. It also affects point selection.
		self.radius = 4

		# Color of points when "object" mode (or other object-based modes) is
		# active and this object is active
		self.color_inactive = (0, 0, 0, 1)

		# Clor of active points when "edit" mode (or other edit-based modes) is
		# active
		self.color_active = (1, 0, 0, 1) # object and point active

	# This function draws a point.
	# - "ctx" is Cairo's context, which is a set of drawing instructions
	# - "active_object" is a boolean parameter which says if the object active
	# - "mode" is current mode ("object", "edit" etc.)
	# - "active_point" is a boolean parameter, which says it the point is
	#   active or not
	# - "center" is canvas position of the object center
	# - "shift" is screen position of top left canvas corner
	# - "zoom" is current canvas zoom
	def draw(self, ctx, active_object, mode, active_point, center, shift,
		zoom):
		# Set color (it depends on object's activity and the mode)
		color = self.select_color(active_object, mode, active_point)
		ctx.set_source_rgba(*color)		

		# Calculate absolute position of the point
		absolute_pos = self.absolute(self.pos, center)

		# Get screen position, not just canvas position
		screen_pos = self.screen_pos(absolute_pos, shift, zoom)

		# Draw point itself. Full (from angle 0 to angle 2*pi) filled circle
		# with absolute coordinates "absolute_pos" of its center and radius
		# "point_radius" will be drawn.
		ctx.arc(screen_pos[0], screen_pos[1], self.radius, 0, 2*math.pi)
		ctx.fill()

	# Calculates screen position from canvas position, canvas shift and zoom
	# - "canvas_pos" is canvas position, which is going to be converted
	# - "shift" is screen position of top left canvas corner
	# - "zoom" is current canvas zoom
	def screen_pos(self, canvas_pos, shift, zoom):
		x = shift[0] + canvas_pos[0] * zoom
		y = shift[1] + canvas_pos[1] * zoom
		return (x, y)

	# This function returns color. which should be used to draw a point. The
	# color depends on it, if the object (which contains the point) is active
	# and what mode is currently on, because activity of point is suposed to be
	# visible only in edit-based modes.
	# - "active_object" is a boolean parameter which says if the object active
	# - "mode" is current mode ("object", "edit" etc.)
	# - "active_point" is a boolean parameter, which says it the point is
	#   active or not
	def select_color(self, active_object, mode, active_point):
		if (active_object and active_point and
					(mode == "edit" or
				mode == "changing_hue" or
				mode == "changing_saturation" or
				mode == "changing_value" or
				mode == "changing_alpha" or
				mode == "moving_points" or
				mode == "rotating_points" or
				mode == "moving_bezier_control_point" or
				mode == "scaling_points" or
				mode == "selecting_box")):
			return self.color_active
		else:
			return self.color_inactive

	# This function returns point position relative to the origin. Inverse
	# function to this function is "absolute".
	# - "pos" is absolute position of the point in format of tuple (x, y)
	# - "origin" is (absolute) position of the origin in format of tuple (x, y)
	def relative(self, pos, origin):
		x = pos[0] - origin[0]
		y = pos[1] - origin[1]
		return (x, y)

	# This function calculates absolute point position from the relative point
	# position to the origin. Inverse function to this function is "relative".
	# - "pos" is relative position of the point in format of tuple (x, y)
	# - "origin" is (absolute) position of the origin in format of tuple (x, y)
	def absolute(self, pos, origin):
		x = pos[0] + origin[0]
		y = pos[1] + origin[1]
		return (x, y)


# =============================================================================
# Start of the program
# =============================================================================
if __name__ == "__main__":
	# Create a window
	Window()

	# Start the main loop
	Gtk.main()