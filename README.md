# Tivom
---
Tivom is a opensource multi-platform application for creating vector images. It is not finished and it is being developed right now.

## About current version (0.0.1)
This is the first version, therefore there are a lot of imperfections. Goal of version 0.0.1 was not to create a great program, but to create program, which will work. That being said, experience from using this software might not be the best (yet). When version 0.0.2 will be introduced, it will be more user-oriented (besides many other changes). Please, be aware, that this is a very early stage of development, so bugs can occur. Also version 0.0.2 might not support back-compatibility.

## Concept
Tivom originally should not be ordinary vector-image editor like Adobe Illustrator or Inkscape. It should be a hybrid between let say Inkscape and Blender. That means, that the concept was to crete sceen with 2D objects, but those objects can have multiple edges (each can have different color and thickness) and face (each can have different color). But unlike Blender also Bezier curves could connect two points. This concept turned out not to be very practical. Therefore in next versions the concept will be more similar with Illustrator-like applications.

## How to install it
As was mentioned in the previous paragraph, this version is not meant to be convinient. This version of Tivom needs to be run as Python 3 script. Please, download `tivom.py` (and optionally `title.png` into the same folder) from commit with **v0.0.1** tag. Then start Tivom simply by writing `python3 tivom.py`.
Please note, that all dependencies need to be installed. In order to run Tivom you have to have installed following

1. Python 3
1. Cairo
1. Gtk, Gdk, GObject (version 3.0)

## Can something actually be drawn in Tivom?
Currently it is not easy, but it certainly is possible. For example here is a link to video of a speed art, which was 100% done in Tivom.

[![Tivom (version 0.0.1) - Grandma speed art](https://img.youtube.com/vi/FkoTVNxZSkU/0.jpg)](https://www.youtube.com/watch?v=FkoTVNxZSkU)

## How to use it
Tivom has at the moment almost no interface, so keyboard must be used. This is going to change in the next version, but for now, knowledge of shortcuts is needed.

### Open project
When you open Tivom, you will see gray screen (with logo). By clicking **n** key dialog for file selection can be opened. This way you can open a project, which already had been created, or create a new project from the scratch. Tivom projects have extension **.tvm**. When **object** mode (see next paragraph) is on, user can return to the welcome screen by pressing **Esc**.

### How to create and manipulate with objects
When application is started **object** mode is turned on. While **object** mode is on, user can manipulate with whole objects, but cannot modify objects properities (shape, color etc.). After a project has been opened and when **object** mode is on, a new object can be created by pressing **n** or **q**. Pressing **n** creates object with only one point, while **q** creates a circle (actually for cubic Bezier curves approximating a circle). By clicking on objects with a mouse, they can be (de)selected. When left **Ctrl** is held more objects can be selected at once. Pressing **a** (de)selects all present objects. Active objects have their "center" (green cross) vissible.

### How to move objects
When **g** is pressed (in **object** mode), all selected objects can be moved by mouse until mouse click occurs.

### How to delete objects
Pressing **d** (in **object** mode) deletes all selected objects.

### How to duplicate object
Pressing **c** (in **object** mode) duplicates all selected objects.

### How to change object order
When **p** is pressed (in **object** mode), all selected objects will be sent to the forground. When **u** is pressed (in **object** mode), all selected objects will be sent to the background. Pressing **o**  (in **object** mode), will send all selected objects one step closer to the forground. Pressing **i**  (in **object** mode), will send all selected objects one step closer to the background.

### How to work with the canvas
Pressing **h** (in **object** mode) opens dialog for changing width and height of the canvas. When **j** is pressed (in **object** mode), canvas can be scaled until mouse click occurs. Note that, this operation does not change dimension of the canvas, only zooms in/out. Pressing **m** (in **object** mode) allows to move the canvas (with all objects with it) until mouse click occurs.

### How to save and export project
Project can be saved by pressing **s** (in **object** mode) and exported to **png** by pressing **b** (in **object** mode).

### How to edit objects
When only one object is selected, **edit** mode can be entered by pressing **Tab**. Press **Tab** again (this time in **edit**) mode to escape **edit** mode back to the **object** mode.

### How to work with points
Working with point is similar to working with objects. They can be (de)selected by click on them (with or without left **Ctrl**). New point can be created by pressing **n** (in **edit** mode). All selected points can be moved after pressing **g** until mouse is clicked. All selected points will be deleted by pressing **d** (in **edit** mode). Pressing **a** (in **edit** mode)will (de)select all points of the object. All select points will be moved towards or away from the object center (the green cross) after pressing **s** (in **edit** mode) until mouse click occurs. All selecte points can be also rotated arouned the object center after pressing **r** (in **edit** mode) until mouse click occurs. It is also possible to flip object's points horizontally (axis is a vertical line, which goes throught the object center) by pressing **h** (in **edit** mode) and vertically (axis is a horizontal line, which goes throught the object center) by pressing **v** (in **edit** mode). Note that moving, scaling, rotating and flipping points will affect also edges and faces
### How to work with edges
Edges always connect two points. There two types of edges - straight lines and (cubic) Bezier curves. When exactly two points are selected a straight line can be created between them by pressing **l** (in **edit** mode) and a Bezier curve by pressing **k** (in **edit** mode). Bezier curves have two control points, which can be selected with mouse and moved after pressing **g** (in **edit** mode) when the control point is selected. It is also possible to add a new point which will be automatically connected with all points, which had been selected. For adding point connected with a straight line press **e** (in **edit** mode) and for point connected with a Bezier curve press **t** (in **edit** mode). Edges can be selected simply by clicking on them. For (de)selection of all edges press **q** (in **edit** mode). When **d** is pressed (in **edit** mode) every selected edge will be deleted. It is possible to switch (selected edges) between straight lines and Bezier curves by pressing **j** (in **edit** mode). Every edge has its thickness. The default thickness is 0 (so it is not visible). Thickness can be changed in the thickness dialog, which can be opened by pressing **i** (in **edit** mode). This will affect all edges, which were selected when the dialog was opened. Also color of edges can be changed in the color dialog, which can be opened by pressing **c** (in **edit** mode). The color dialog contains 4 color bars - for changing hue, saturation, value and alpha. Every color bar has its cursor, which determines the value and can by draged by mouse. When the color dialog is opened, new color can be also set simply by clicking on canvas on the wanted color. The color dialog can be closed by pressing **c** again.

### How to work with faces
Faces can fill the space bordered by edges. When edges, which can form a perfect loop (it can be closed and no edge remains), are selected and **f** (in **edit** mode) is pressed, face will be created. Faces can be selected by click on them (optionally with left **Ctrl** for selecting more). All selected faces will be deleted by pressing **d** (in **edit** mode). Color of faces can be changed with the color dialog (see details in previous paragraph).

### How to undo/redo
It is possible to undo/redo in every mode. Pressing **z** will undo, while pressing **y** will redo.